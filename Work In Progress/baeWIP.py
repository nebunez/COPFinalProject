__author__ = 'ebryc_000'
"""
jrWIP.py
John Ryan's Prototype module for Group Awesome's COP Final Project.
This prototype focuses on the GUI elements for the project.
"""

import sys
import pygame
from pygame.locals import *


### PYGAME SETUP
pygame.init()
pygame.mouse.set_visible(False)
pygame.mixer.init(44100, -16, 2, 2048)
GAMEMUSIC = pygame.mixer.music
MAINCLOCK = pygame.time.Clock()

### SCREEN SETUP
pygame.display.set_caption("JR WIP")

SCREEN_INFO = pygame.display.Info()
WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

GAMEWINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 0)

GWX = GAMEWINDOW.get_rect().centerx
GWY = GAMEWINDOW.get_rect().centery
GWC = GAMEWINDOW.get_rect().center
GWL = GAMEWINDOW.get_rect().left
GWR = GAMEWINDOW.get_rect().right
GWT = GAMEWINDOW.get_rect().top
GWB = GAMEWINDOW.get_rect().bottom


### COLORS AND FONTS
BLACK = (0, 0, 0)
RED = (255, 0, 0)
DARK_RED = (155, 0, 0)
GREEN = (0, 255, 0)
DARK_GREEN = (0, 155, 0)
BLUE = (0, 0, 255)
PURPLE = (155, 0, 200)
WHITE = (255, 255, 255)

def textObject(txt, font, color):
    text = font.render(txt, True, color)
    return text, text.get_rect()

MENUFONT = pygame.font.SysFont(None, 30)


### IMAGES
MENUBG = pygame.image.load('Images/MainMenu.png')

IMGSTARTOFF = pygame.image.load('Images/StartOff.png')
IMGSTARTON = pygame.image.load('Images/StartOn.png')
IMGSTARTPRESSED = pygame.image.load('Images/StartPressed.png')
IMGSTARTSIZE = IMGSTARTOFF.get_size()

IMGQUITOFF = pygame.image.load('Images/QuitOff.png')
IMGQUITON = pygame.image.load('Images/QuitOn.png')
IMGQUITPRESSED = pygame.image.load('Images/QuitPressed.png')
IMGQUITSIZE = IMGSTARTOFF.get_size()

CURSORICON = pygame.image.load('Images/cursor.gif')
cursorRect = CURSORICON.get_rect()

PLAYERSHIP = pygame.image.load('Images/PlayerShip.jpg')
BULLET  = pygame.image.load('Images/Bullet.png')


### SPRITES AND GROUPS
class Ship(pygame.sprite.Sprite):
    """Our ship"""
    image = PLAYERSHIP
    image.set_colorkey((0,0,0))
    image = image.convert_alpha()

    def __init__(self, startpos, num):
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.pos = startpos
        self.image = Ship.image
        self.rect = self.image.get_rect()
        self.radius = 50
        self.number = num

    def update(self):
        self.rect.center = self.pos

class Bullet(pygame.sprite.Sprite):
    """What we shoot"""
    image = BULLET
    image.set_colorkey((0,0,0))
    image = image.convert_alpha()

    def __init__(self, startpos):
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.pos = startpos
        self.image = Bullet.image
        self.rect = self.image.get_rect()
        self.radius = 5

    def update(self):
        self.rect.center = self.pos

playerGroup = pygame.sprite.Group()
bulletGroup = pygame.sprite.Group()
crashGroup = pygame.sprite.Group()
Ship.groups = playerGroup
Bullet.groups = bulletGroup


### BUTTONS
BUTTONW = 170
BUTTONH = 40

buttonStart = {'ImgOff': IMGSTARTOFF,
               'ImgOn': IMGSTARTON,
               'ImgPressed': IMGSTARTPRESSED,
               'Rect': ((GWX - BUTTONW * 2, GWY - 50), IMGSTARTSIZE)}

buttonQuit = {'ImgOff': IMGQUITOFF,
              'ImgOn': IMGQUITON,
              'ImgPressed': IMGQUITPRESSED,
              'Rect': ((GWX - BUTTONW * 2, GWY + 100), IMGQUITSIZE)}

buttonMenuQuit = {'ImgOff': IMGQUITOFF,
                  'ImgOn': IMGQUITON,
                  'ImgPressed': IMGQUITPRESSED,
                  'Rect': ((GWX - (IMGQUITSIZE[0]/2), GWY + IMGQUITSIZE[1]), IMGQUITSIZE)}


### FUNCTIONS
def main():
    """Main game engine."""
    global GAMEWINDOW, MAINCLOCK, GAMEMUSIC, MENUBG
    GAMEMUSIC.load('Audio/MenuMusic.mp3')

    while True:
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos

        if not GAMEMUSIC.get_busy():
            GAMEMUSIC.play(-1, 22)

        # SCREEN REFRESH
        GAMEWINDOW.blit(MENUBG, (0,0))

        if button(buttonStart, mousePos, click1):
            if pygame.event.get(MOUSEBUTTONUP):
                shooty()
        if button(buttonQuit, mousePos, click1):
            if pygame.event.get(MOUSEBUTTONUP):
                quitGame()

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    quitGame()

        GAMEWINDOW.blit(CURSORICON, mousePos)
        pygame.display.update()
        MAINCLOCK.tick(60)

def button(but, cursorPos, click):
    """Define button behaviours."""
    butRect = pygame.Rect(but['Rect'])

    if (butRect.left < cursorPos[0] < butRect.right and
        butRect.top < cursorPos[1] < butRect.bottom):
        GAMEWINDOW.blit(but['ImgOn'], but['Rect'])
        if click == 1:
            GAMEWINDOW.blit(but['ImgPressed'], but['Rect'])
            return True
    else:
        GAMEWINDOW.blit(but['ImgOff'], but['Rect'])

def inGameMenu(cursorPos, click):
    """Menu overlay during game."""
    print('IN MENU')
    menuWindow = pygame.Surface((500, 250))
    menuWindow.set_alpha(128)
    menuWindow.fill((40, 40, 50))
    GAMEWINDOW.blit(menuWindow, (GWX - 250, GWY - 125))

    if button(buttonMenuQuit, cursorPos, click):
        pygame.time.delay(100)
        if pygame.event.get(MOUSEBUTTONUP):
            return True

def startGame():
    """Run actual game here."""
    global GAMEWINDOW, MAINCLOCK, GAMEMUSIC

    tempText, tempRect = textObject("THE CAKE IS A LIE", MENUFONT, BLACK)
    temp2Text, temp2Rect = textObject("THE LIE IS A CAKE", MENUFONT, RED)
    tempRect.center = GWC
    temp2Rect.center = GWC

    flasher = 0
    menuOn = False
    GAMEMUSIC.stop()

    while True:
        print('IN GAME')
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos

        for event in pygame.event.get():
            # Check for Quit
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE or event.key == K_SPACE:

                    return None
                if event.key == K_m:
                    menuOn = not menuOn

        # SCREEN REFRESH
        if flasher < 30:
            GAMEWINDOW.fill(BLUE)
            GAMEWINDOW.blit(tempText, tempRect)
            flasher += 1
        elif 30 <= flasher < 60:
            GAMEWINDOW.fill(WHITE)
            GAMEWINDOW.blit(temp2Text, tempRect)
            flasher += 1
        else:
            flasher = 0

        if menuOn:
            if inGameMenu(mousePos, click1):
                quitGame()

        GAMEWINDOW.blit(CURSORICON, mousePos)
        pygame.display.update()
        MAINCLOCK.tick(60)

def shooty():
    """Shooter Prototype."""
    global GAMEWINDOW, MAINCLOCK, GAMEMUSIC

    menuOn = False
    GAMEMUSIC.stop()

    player = Ship( [GWX, GWY], 0)
    bullet = Bullet( [0,0])
    computer = Ship( [650, 45], 1)


    moveUp = False
    moveDown = False
    moveLeft = False
    moveRight = False
    moveSpeed = 5

    turnLeft = False
    turnRight = False
    turnSpeed = 90

    shoot = False
    shootCounter = 0

    print(GWT)
    print(GWB)
    print(GWL)
    print(GWR)
    while True:
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos
        bullPos = [player.pos[0], player.pos[1] - 3]
        print(player.pos)
        print(computer.pos)

        if player.pos == computer.pos:
            print('crash')

        if bullet.pos == computer.pos:
            print('hit')


        for event in pygame.event.get():
            # Check for Quit
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return None
                if event.key == K_m:
                    menuOn = not menuOn
                # Movement Input
                if event.key == K_UP:
                    moveDown = False
                    moveUp = True
                if event.key == K_DOWN:
                    moveUp = False
                    moveDown = True
                if event.key == K_LEFT:
                    moveRight = False
                    moveLeft = True
                if event.key == K_RIGHT:
                    moveLeft = False
                    moveRight = True
                if event.key == K_SPACE:
                    shoot = True
                if event.key == K_q:
                    turnRight = False
                    turnLeft = True
                if event.key == K_e:
                    turnLeft = False
                    turnRight = True

            if event.type == KEYUP:
                if event.key == K_UP:
                    moveUp = False
                if event.key == K_DOWN:
                    moveDown = False
                if event.key == K_LEFT:
                    moveLeft = False
                if event.key == K_RIGHT:
                    moveRight = False
                if event.key == K_q:
                    turnLeft = False
                if event.key == K_e:
                    turnRight = False

        if moveUp and player.pos[1] > GWT:
            #posy -= moveSpeed
            player.pos[1] -= moveSpeed
        if moveDown and player.pos[1] < GWB:
            #posy += moveSpeed
            player.pos[1] += moveSpeed
        if moveLeft and player.pos[0] > GWL:
            #posx -= moveSpeed
            player.pos[0] -= moveSpeed
        if moveRight and player.pos[0] < GWR:
            #posx += moveSpeed
            player.pos[0] += moveSpeed
        #if turnLeft:
            #pShip = pygame.transform.rotozoom(player.image, -turnSpeed, 1)
        #if turnRight:
            #pShip = pygame.transform.rotozoom(player.image, turnSpeed, 1)

        # SCREEN REFRESH
        GAMEWINDOW.fill(BLACK)
        GAMEWINDOW.blit(player.image, player.pos)
        GAMEWINDOW.blit(computer.image, computer.pos)

        if not shoot:
            bullx = player.pos[0]
            bully = player.pos[1]
        if shoot:
            if shootCounter == 0:
                shootCounter = 1
            elif 0 < shootCounter < 60:
                bully -= 5
                GAMEWINDOW.blit(bullet.image, (bullx, bully))
                shootCounter += 1
            else:
                shootCounter = 0
                shoot = False

        if menuOn:
            if inGameMenu(mousePos, click1):
                quitGame()

        GAMEWINDOW.blit(CURSORICON, mousePos)
        pygame.display.update()
        MAINCLOCK.tick(60)

def quitGame():
    """Exit pygame and system."""
    pygame.quit()
    sys.exit()

### Run Game
if __name__ == "__main__":
    shooty()
