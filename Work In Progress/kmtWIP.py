"""
jrWIP.py
John Ryan's Prototype module for Group Awesome's COP Final Project.
This prototype focuses on the GUI elements for the project.
"""

import sys
import pygame
import math
from pygame.locals import *


### PYGAME SETUP
pygame.init()
pygame.mouse.set_visible(False)
pygame.mixer.init(44100, -16, 2, 2048)
GAMEMUSIC = pygame.mixer.music
MAINCLOCK = pygame.time.Clock()

### SCREEN SETUP
pygame.display.set_caption("JR WIP")

SCREEN_INFO = pygame.display.Info()
WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

GAMEWINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 0)

GWX = GAMEWINDOW.get_rect().centerx
GWY = GAMEWINDOW.get_rect().centery
GWC = GAMEWINDOW.get_rect().center
GWL = GAMEWINDOW.get_rect().left
GWR = GAMEWINDOW.get_rect().right
GWT = GAMEWINDOW.get_rect().top
GWB = GAMEWINDOW.get_rect().bottom

### MATH
FRICTION = 0
GRAD = math.pi / 180


### COLORS AND FONTS
BLACK = (0, 0, 0)
RED = (255, 0, 0)
DARK_RED = (155, 0, 0)
GREEN = (0, 255, 0)
DARK_GREEN = (0, 155, 0)
BLUE = (0, 0, 255)
PURPLE = (155, 0, 200)
WHITE = (255, 255, 255)

def textObject(txt, font, color):
    text = font.render(txt, True, color)
    return text, text.get_rect()

MENUFONT = pygame.font.SysFont(None, 30)


### IMAGES
MENUBG = pygame.image.load('Images/MainMenu.png')

IMGSTARTOFF = pygame.image.load('Images/StartOff.png')
IMGSTARTON = pygame.image.load('Images/StartOn.png')
IMGSTARTPRESSED = pygame.image.load('Images/StartPressed.png')
IMGSTARTSIZE = IMGSTARTOFF.get_size()

IMGQUITOFF = pygame.image.load('Images/QuitOff.png')
IMGQUITON = pygame.image.load('Images/QuitOn.png')
IMGQUITPRESSED = pygame.image.load('Images/QuitPressed.png')
IMGQUITSIZE = IMGSTARTOFF.get_size()

CURSORICON = pygame.image.load('Images/cursor.gif')
cursorRect = CURSORICON.get_rect()

PLAYERSHIP = pygame.image.load('Images/PlayerShip.jpg')
BULLET  = pygame.image.load('Images/Bullet.png')


### SPRITES AND GROUPS
playerGroup = pygame.sprite.Group()

class Ship(pygame.sprite.Sprite):
    """Our ship"""
    image = PLAYERSHIP

    def __init__(self):
        self.groups = playerGroup
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.area = GAMEWINDOW.get_rect()
        self.dx = 0
        self.dy = 0
        self.boostSpeed =10
        self.boostMax = 0.9
        self.boostMin = 0.4
        self.boostTime = 0
        ###
        self.hitPoints = float(100)
        self.hitPointsMax = float(100)
        self.image = PLAYERSHIP
        self.pos = [GWX, GWY]
        self.rect = self.image.get_rect()
        self.angle = 0
        self.speed = 10
        self.rotateSpeed = 1
        self.coolDownTime = 0.08
        self.coolDown = 0
        self.damage = 5
        self.shots = 0
        self.radius = self.image.get_width()/2.0
        self.mass = 400

    def kill(self):
        pygame.sprite.Sprite.kill(self)

    def speedCheck(self):
        if abs(self.dx) > 0:
            self.dx *= FRICTION
        if abs(self.dy) > 0:
            self.dy *= FRICTION

    def areaCheck(self):
        if not self.area.contains(self.rect):
            if self.pos[0] + self.rect.width/2 > self.area.right:
                self.pos[0] = self.area.right - self.rect.width/2
                self.dx *= -0.5
            if self.pos[0] - self.rect.width/2 < self.area.left:
                self.pos[0] = self.area.left + self.rect.width/2
                self.dx *= 0.5
            if self.pos[1] + self.rect.height/2 > self.area.bottom:
                self.pos[1] = self.area.bottom - self.rect.height/2
                self.dx *= 0.5
            if self.pos[1] - self.rect.height/2 < self.area.top:
                self.pos[1] = self.area.top - self.rect.height/2
                self.dx *= 0.5

    def update(self, time):
        pressedKeys = pygame.key.get_pressed()
        self.ddx = 0.0
        self.ddy = 0.0
        if pressedKeys[pygame.K_w]:
            self.ddx = -math.sin(self.angle * GRAD)
            self.ddy = -math.cos(self.angle * GRAD)
        if pressedKeys[pygame.K_s]:
            self.ddx = +math.sin(self.angle * GRAD)
            self.ddy = +math.cos(self.angle * GRAD)
        self.dx += self.ddx * self.speed
        self.dy += self.ddy * self.speed
        self.pos[0] += self.dx * time
        self.pos[1] += self.dy * time
        self.areaCheck()
        if pressedKeys[pygame.K_q]:
            self.angle += self.rotateSpeed
        if pressedKeys[pygame.K_e]:
            self.angle -= self.rotateSpeed
        self.oldCenter = self.rect.center
        self.image = pygame.transform.rotate(self.image, self.angle)
        self.rect = self.image.get_rect()
        self.rect.center = self.oldCenter
        self.rect.centerx = round(self.pos[0], 0)
        self.rect.centery = round(self.pos[1], 0)


class Bullet(pygame.sprite.Sprite):
    """What we shoot"""
    image = BULLET
    image.set_colorkey((0,0,0))
    image = image.convert_alpha()

    def __init__(self, startpos):
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.pos = startpos
        self.image = Bullet.image
        self.rect = self.image.get_rect()
        self.radius = 5

    def update(self):
        self.rect.center = self.pos

Ship.groups = playerGroup
Bullet.groups = playerGroup


### BUTTONS
class Button():
    """Button Class"""
    def __init__(self, imgOff, imgOn, imgPressed, cent=(0,0)):
        self.imageOff = imgOff
        self.imageOn = imgOn
        self.imagePressed = imgPressed
        self.size = imgOff.get_size()
        self.rect = imgOff.get_rect()
        #self.rect = pygame.Rect(left - self.size[0], top - self.size[1], self.size[0], self.size[1])
        self.rect.center = cent

    def update(self, curs, click):
        cursorPos = curs

        if (self.rect.left < cursorPos[0] < self.rect.right and
            self.rect.top < cursorPos[1] < self.rect.bottom):
            GAMEWINDOW.blit(self.imageOn, self.rect)
            if click == 1:
                GAMEWINDOW.blit(self.imagePressed, self.rect)
                return True
        else:
            GAMEWINDOW.blit(self.imageOff, self.rect)


### FUNCTIONS
def main():
    """Main game engine."""
    global GAMEWINDOW, MAINCLOCK, GAMEMUSIC, MENUBG
    GAMEMUSIC.load('Audio/MenuMusic.mp3')
    buttonStart = Button(IMGSTARTOFF, IMGSTARTON, IMGSTARTPRESSED, (GWX * 0.66, GWY - 50))
    buttonQuit = Button(IMGQUITOFF, IMGQUITON, IMGQUITPRESSED, (GWX * 0.66, GWY + 100))

    while True:
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos

        if not GAMEMUSIC.get_busy():
            GAMEMUSIC.play(-1, 22)

        # SCREEN REFRESH
        GAMEWINDOW.blit(MENUBG, (0,0))

        if buttonStart.update(mousePos, click1):
            if pygame.event.get(MOUSEBUTTONUP):
                shooty()
        if buttonQuit.update(mousePos, click1):
            if pygame.event.get(MOUSEBUTTONUP):
                quitGame()

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    quitGame()

        GAMEWINDOW.blit(CURSORICON, mousePos)
        pygame.display.update()
        MAINCLOCK.tick(60)

def inGameMenu(cursorPos, click):
    """Menu overlay during game."""
    buttonMenuQuit = Button(IMGQUITOFF, IMGQUITON, IMGQUITPRESSED, (GWX, GWY))
    menuWindow = pygame.Surface((500, 250))
    menuWindow.set_alpha(128)
    menuWindow.fill((40, 40, 50))
    GAMEWINDOW.blit(menuWindow, (GWX - 250, GWY - 125))

    if buttonMenuQuit.update(cursorPos, click):
        pygame.time.delay(100)
        if pygame.event.get(MOUSEBUTTONUP):
            return True

def startGame():
    """Run actual game here."""
    global GAMEWINDOW, MAINCLOCK, GAMEMUSIC

    tempText, tempRect = textObject("THE CAKE IS A LIE", MENUFONT, BLACK)
    temp2Text, temp2Rect = textObject("THE LIE IS A CAKE", MENUFONT, RED)
    tempRect.center = GWC
    temp2Rect.center = GWC

    flasher = 0
    menuOn = False
    GAMEMUSIC.stop()

    while True:
        print('IN GAME')
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos

        for event in pygame.event.get():
            # Check for Quit
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE or event.key == K_SPACE:
                    
                    return None
                if event.key == K_m:
                    menuOn = not menuOn

        # SCREEN REFRESH
        if flasher < 30:
            GAMEWINDOW.fill(BLUE)
            GAMEWINDOW.blit(tempText, tempRect)
            flasher += 1
        elif 30 <= flasher < 60:
            GAMEWINDOW.fill(WHITE)
            GAMEWINDOW.blit(temp2Text, tempRect)
            flasher += 1
        else:
            flasher = 0

        if menuOn:
            if inGameMenu(mousePos, click1):
                quitGame()

        GAMEWINDOW.blit(CURSORICON, mousePos)
        pygame.display.update()
        MAINCLOCK.tick(60)

def shooty():
    """Shooter Prototype."""
    global GAMEWINDOW, MAINCLOCK, GAMEMUSIC

    GAMEMUSIC.stop()
    menuOn = False
    player = Ship()
    seconds = MAINCLOCK.tick(60) / 1000

    while True:
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos
        print(player.pos)

        for event in pygame.event.get():
            if event.type == QUIT:
                pygaem.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    quitGame()
                if event.key == K_m:
                    menuOn = not menuOn

        GAMEWINDOW.fill(BLACK)

        if menuOn:
            if inGameMenu(mousePos, click1):
                quitGame()

        playerGroup.update(seconds)
        playerGroup.draw(GAMEWINDOW)
        GAMEWINDOW.blit(CURSORICON, mousePos)
        pygame.display.update()
        MAINCLOCK.tick(60)

def quitGame():
    """Exit pygame and system."""
    pygame.quit()
    sys.exit()

### Run Game
if __name__ == "__main__":
    main()