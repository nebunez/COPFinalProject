"""
jrWIP.py
John Ryan's Prototype module for Group Awesome's COP Final Project.
This prototype focuses on the GUI elements for the project.
"""

import sys
import pygame
import math
from pygame.locals import *



### PYGAME SETUP
pygame.init()
pygame.mouse.set_visible(False)
pygame.mixer.init(44100, -16, 2, 2048)
GAMEMUSIC = pygame.mixer.music
MAINCLOCK = pygame.time.Clock()

### SCREEN SETUP
pygame.display.set_caption("JR WIP")

SCREEN_INFO = pygame.display.Info()
WINDOW_WIDTH = 640
WINDOW_HEIGHT = 480

GAMEWINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 0)

GWX = GAMEWINDOW.get_rect().centerx
GWY = GAMEWINDOW.get_rect().centery
GWC = GAMEWINDOW.get_rect().center
GWL = GAMEWINDOW.get_rect().left
GWR = GAMEWINDOW.get_rect().right
GWT = GAMEWINDOW.get_rect().top
GWB = GAMEWINDOW.get_rect().bottom

### MATH
FRICTION = 0.97
RAD = math.pi / 180


### COLORS AND FONTS
BLACK = (0, 0, 0)
RED = (255, 0, 0)
DARK_RED = (155, 0, 0)
GREEN = (0, 255, 0)
DARK_GREEN = (0, 155, 0)
BLUE = (0, 0, 255)
PURPLE = (155, 0, 200)
WHITE = (255, 255, 255)

def textObject(txt, font, color):
    text = font.render(txt, True, color)
    return text, text.get_rect()

MENUFONT = pygame.font.SysFont(None, 30)
COUNTFONT = pygame.font.SysFont(None, 72)


### IMAGES
MENUBG = pygame.image.load('Images/MainMenu.png')

IMGSTARTOFF = pygame.image.load('Images/StartOff.png')
IMGSTARTON = pygame.image.load('Images/StartOn.png')
IMGSTARTPRESSED = pygame.image.load('Images/StartPressed.png')
IMGSTARTSIZE = IMGSTARTOFF.get_size()

IMGQUITOFF = pygame.image.load('Images/QuitOff.png')
IMGQUITON = pygame.image.load('Images/QuitOn.png')
IMGQUITPRESSED = pygame.image.load('Images/QuitPressed.png')
IMGQUITSIZE = IMGSTARTOFF.get_size()

CURSORICON = pygame.image.load('Images/cursor.gif')
cursorRect = CURSORICON.get_rect()

PLAYERSHIP = pygame.image.load('Images/PlayerShip.jpg')
BULLET  = pygame.image.load('Images/Bullet.png')


### SPRITES AND GROUPS
playerGroup = pygame.sprite.Group()
bulletGroup = pygame.sprite.Group()

class Ship(pygame.sprite.Sprite):
    """Our ship"""
    image = PLAYERSHIP
    image.set_colorkey((0,0,0))
    image = image.convert_alpha()

    def __init__(self):
        self.count5, self.count5rect = textObject("5", COUNTFONT, WHITE)
        self.count4, self.count4rect = textObject("4", COUNTFONT, WHITE)
        self.count3, self.count3rect = textObject("3", COUNTFONT, WHITE)
        self.count2, self.count2rect = textObject("2", COUNTFONT, WHITE)
        self.count1, self.count1rect = textObject("1", COUNTFONT, WHITE)
        self.count5rect.center = (GWX, GWY - 50)
        self.count4rect.center = (GWX, GWY - 50)
        self.count3rect.center = (GWX, GWY - 50)
        self.count2rect.center = (GWX, GWY - 50)
        self.count1rect.center = (GWX, GWY - 50)
        self.groups = playerGroup
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.area = GAMEWINDOW.get_rect()
        self.dx = 0
        self.dy = 0
        self.canShoot = False
        self.coolDown = 0
        self.image = Ship.image
        self.pos = [GWX, GWY]
        self.rect = self.image.get_rect()
        self.radius = self.image.get_width() / 2.0
        self.speed = 5
        self.angle = 0
        self.rotateSpeed = 3
        self.shots = 0
        self.canMove = False
        self.counter = 300

    def kill(self):
        pygame.sprite.Sprite.kill(self)

    def speedCheck(self):
        if abs(self.dx) > 0:
            self.dx *= FRICTION
        if abs(self.dy) > 0:
            self.dy *= FRICTION

    def areaCheck(self):
        if not self.area.contains(self.rect):
            if self.pos[0] + self.rect.width/2 > self.area.right:
                self.pos[0] = self.area.right - self.rect.width/2
                self.dx *= -0.5
            if self.pos[0] - self.rect.width/2 < self.area.left:
                self.pos[0] = self.area.left + self.rect.width/2
                self.dx *= 0.5
            if self.pos[1] + self.rect.height/2 > self.area.bottom:
                self.pos[1] = self.area.bottom - self.rect.height/2
                self.dx *= 0.5
            if self.pos[1] - self.rect.height/2 < self.area.top:
                self.pos[1] = self.area.top + self.rect.height/2
                self.dx *= 0.5

    def shoot(self):
        #print("SHOOT")
        bullet = Bullet([self.pos[0], self.pos[1]], self.angle)

    def update(self, time):
        if self.counter > 0:
            self.counter -= 1
            GAMEWINDOW.blit(self.image, self.rect,)
            self.rect.center = self.pos
            if self.counter>240:
                GAMEWINDOW.blit(self.count5, self.count5rect)
            elif self.counter>180:
                GAMEWINDOW.blit(self.count4, self.count4rect)
            elif self.counter>120:
                GAMEWINDOW.blit(self.count3, self.count3rect)
            elif self.counter>60:
                GAMEWINDOW.blit(self.count2, self.count2rect)
            elif self.counter>0:
                GAMEWINDOW.blit(self.count1, self.count1rect)
        elif self.counter == 0:
            self.counter = -1
            self.canMove = True
            GAMEWINDOW.blit(self.image, self.rect,)
        else:
            print("MOVE IT!")
            pressedKeys = pygame.key.get_pressed()
            self.ddx = 0.0
            self.ddy = 0.0
            #---Input---
            if pressedKeys[pygame.K_w]:
                self.ddx = -math.sin(self.angle * RAD)
                self.ddy = -math.cos(self.angle * RAD)
            if pressedKeys[pygame.K_s]:
                self.ddx = +math.sin(self.angle * RAD)
                self.ddy = +math.cos(self.angle * RAD)
            if pressedKeys[pygame.K_q]:
                self.angle += self.rotateSpeed
            if pressedKeys[pygame.K_e]:
                self.angle -= self.rotateSpeed
            '''if pressedKeys[pygame.K_SPACE]:
                if self.coolDown == 0:
                    self.canShoot = True
                    self.coolDown = 1'''
            #---Shoot Cooldown---
            if self.canShoot:
                self.shoot()
                self.canShoot = not self.canShoot
            elif not self.canShoot and 0 < self.coolDown < 60:
                self.coolDown += 1
            else:
                self.coolDown = 0
            #---Movement---
            self.dx += self.ddx * self.speed
            self.dy += self.ddy * self.speed
            self.speedCheck()
            self.pos[0] += self.dx * time
            self.pos[1] += self.dy * time
            self.areaCheck()
            #---Draw Object---
            self.oldCenter = self.rect.center
            self.newImage = pygame.transform.rotate(self.image, self.angle)
            self.rect = self.newImage.get_rect()
            self.rect.center = self.oldCenter
            self.rect.centerx = round(self.pos[0], 0)
            self.rect.centery = round(self.pos[1], 0)
            GAMEWINDOW.blit(self.newImage, self.rect)

class Bullet(pygame.sprite.Sprite):
    """What we shoot"""
    image = BULLET
    image.set_colorkey((0,0,0))
    image = image.convert_alpha()

    def __init__(self, startpos, ang):
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.pos = startpos
        self.angle = ang
        self.image = Bullet.image
        self.rect = self.image.get_rect()
        self.dx = 0
        self.dy = 0
        self.speed = 6
        self.counter = 0

    def update(self):
        self.ddx = 0.0
        self.ddy = 0.0
        self.ddx = -math.sin(self.angle * RAD)
        self.ddy = -math.cos(self.angle * RAD)
        if self.counter < 60:
            #print(self.counter)
            self.dx = self.ddx * self.speed
            self.dy = self.ddy * self.speed
            self.pos[0] += self.dx
            self.pos[1] += self.dy
            self.rect.centerx = round(self.pos[0], 0)
            self.rect.centery = round(self.pos[1], 0)
            self.counter += 1
        else:
            #print("BULLET KILL")
            pygame.sprite.Sprite.kill(self)
        GAMEWINDOW.blit(self.image, self.rect)

Ship.groups = playerGroup
Bullet.groups = bulletGroup


def doRectsOverlap(rect1, rect2):
    for a, b in [(rect1, rect2), (rect2, rect1)]:
        # Check if a's corners are inside b
        if ((isPointInsideRect(a.left, a.top, b)) or
            (isPointInsideRect(a.left, a.bottom, b)) or
            (isPointInsideRect(a.right, a.top, b)) or
            (isPointInsideRect(a.right, a.bottom, b))):
            return True
    return False

### BUTTONS
class Button():
    """Button Class"""
    def __init__(self, imgOff, imgOn, imgPressed, cent=(0,0)):
        self.imageOff = imgOff
        self.imageOn = imgOn
        self.imagePressed = imgPressed
        self.size = imgOff.get_size()
        self.rect = imgOff.get_rect()
        #self.rect = pygame.Rect(left - self.size[0], top - self.size[1], self.size[0], self.size[1])
        self.rect.center = cent

    def update(self, curs, click):
        cursorPos = curs

        if (self.rect.left < cursorPos[0] < self.rect.right and
            self.rect.top < cursorPos[1] < self.rect.bottom):
            GAMEWINDOW.blit(self.imageOn, self.rect)
            if click == 1:
                GAMEWINDOW.blit(self.imagePressed, self.rect)
                return True
        else:
            GAMEWINDOW.blit(self.imageOff, self.rect)


### FUNCTIONS
def main():
    """Main game engine."""
    global GAMEWINDOW, MAINCLOCK, GAMEMUSIC, MENUBG
    buttonStart = Button(IMGSTARTOFF, IMGSTARTON, IMGSTARTPRESSED, (GWX * 0.66, GWY - 50))
    buttonQuit = Button(IMGQUITOFF, IMGQUITON, IMGQUITPRESSED, (GWX * 0.66, GWY + 100))

    while True:
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos

        if not GAMEMUSIC.get_busy():
            GAMEMUSIC.load('Audio/MenuMusic.wav')
            GAMEMUSIC.play(-1)

        # SCREEN REFRESH
        GAMEWINDOW.blit(MENUBG, (0,0))

        if buttonStart.update(mousePos, click1):
            if pygame.event.get(MOUSEBUTTONUP):
                shooty()
        if buttonQuit.update(mousePos, click1):
            if pygame.event.get(MOUSEBUTTONUP):
                quitGame()

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    quitGame()

        GAMEWINDOW.blit(CURSORICON, mousePos)
        pygame.display.update()
        MAINCLOCK.tick(60)


def inGameMenu(cursorPos, click):
    """Menu overlay during game."""
    buttonMenuQuit = Button(IMGQUITOFF, IMGQUITON, IMGQUITPRESSED, (GWX, GWY))
    menuWindow = pygame.Surface((500, 250))
    menuWindow.set_alpha(128)
    menuWindow.fill((40, 40, 50))
    GAMEWINDOW.blit(menuWindow, (GWX - 250, GWY - 125))

    if buttonMenuQuit.update(cursorPos, click):
        pygame.time.delay(100)
        if pygame.event.get(MOUSEBUTTONUP):
            return True

def startGame():
    """Run actual game here. !! NOT USED YET !!"""
    global GAMEWINDOW, MAINCLOCK, GAMEMUSIC

    tempText, tempRect = textObject("THE CAKE IS A LIE", MENUFONT, BLACK)
    temp2Text, temp2Rect = textObject("THE LIE IS A CAKE", MENUFONT, RED)
    tempRect.center = GWC
    temp2Rect.center = GWC

    flasher = 0
    menuOn = False
    GAMEMUSIC.stop()

    while True:
        print('IN GAME')
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos

        for event in pygame.event.get():
            # Check for Quit
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE or event.key == K_SPACE:
                    
                    return None
                if event.key == K_m:
                    menuOn = not menuOn

        # SCREEN REFRESH
        if flasher < 30:
            GAMEWINDOW.fill(BLUE)
            GAMEWINDOW.blit(tempText, tempRect)
            flasher += 1
        elif 30 <= flasher < 60:
            GAMEWINDOW.fill(WHITE)
            GAMEWINDOW.blit(temp2Text, tempRect)
            flasher += 1
        else:
            flasher = 0

        if menuOn:
            if inGameMenu(mousePos, click1):
                quitGame()

        GAMEWINDOW.blit(CURSORICON, mousePos)
        pygame.display.update()
        MAINCLOCK.tick(60)

def shooty():
    """Shooter Prototype."""
    global GAMEWINDOW, MAINCLOCK, GAMEMUSIC

    instruct, instructRect = textObject("Escape to return to menu, Press 'M' to quit",
                                        MENUFONT, WHITE)
    instructRect.centerx = GWX
    instructRect.top = GWT + 5

    GAMEMUSIC.stop()
    GAMEMUSIC.load('Audio/GameMusic.wav')

    menuOn = False
    player = Ship()
    seconds = MAINCLOCK.tick(60) / 1000

    while True:
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos
        #print(player.pos)
        #print(player.coolDown)

        if not GAMEMUSIC.get_busy():
            GAMEMUSIC.play(-1)

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    #---Remove game objects from scene before returning to main menu.
                    playerGroup.empty()
                    bulletGroup.empty()
                    GAMEMUSIC.stop()
                    return None
                if event.key == K_m:
                    menuOn = not menuOn
                if event.key == K_SPACE and player.coolDown == 0:
                    #print("PLEASE SHOOT")
                    player.canShoot = True
                    player.coolDown = 1

        GAMEWINDOW.fill(BLACK)

        if menuOn:
            if inGameMenu(mousePos, click1):
                quitGame()
            GAMEWINDOW.blit(CURSORICON, mousePos)

        playerGroup.update(seconds)
        bulletGroup.update()
        GAMEWINDOW.blit(instruct, instructRect)
        pygame.display.update()
        MAINCLOCK.tick(60)

def quitGame():
    """Exit pygame and system."""
    pygame.quit()
    sys.exit()

### Run Game
if __name__ == "__main__":
    main()