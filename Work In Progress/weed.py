"""
weed.py
Ladies man69's Prototype module for Group Awesome's COP Final Project.
This prototype focuses on the DANK elements for the project.
"""

import sys
import pygame
import math
import random
from pygame.locals import *


### PYGAME SETUP
pygame.init()
pygame.mouse.set_visible(False)
pygame.mixer.init(44100, -16, 2, 2048)
MIXER = pygame.mixer.music
MAINCLOCK = pygame.time.Clock()
#---Joysticks---
joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
for joy in joysticks:
    joy.init()

### SCREEN SETUP
pygame.display.set_caption("JR WIP")

SCREEN_INFO = pygame.display.Info()
WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

GAMEWINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 0)

GWX = GAMEWINDOW.get_rect().centerx
GWY = GAMEWINDOW.get_rect().centery
GWC = GAMEWINDOW.get_rect().center
GWL = GAMEWINDOW.get_rect().left
GWR = GAMEWINDOW.get_rect().right
GWT = GAMEWINDOW.get_rect().top
GWB = GAMEWINDOW.get_rect().bottom

### MATH
FRICTION = 0.97
RAD = math.pi / 180


### COLORS AND FONTS
BLACK = (0, 0, 0)
GREY = (25, 25, 25)
RED = (255, 0, 0)
DARK_RED = (155, 0, 0)
GREEN = (0, 255, 0)
DARK_GREEN = (0, 155, 0)
BLUE = (0, 0, 255)
PURPLE = (155, 0, 200)
WHITE = (255, 255, 255)

def Pallete01():
    return random.randint(0, 255), 0, 0

def Pallete02():
    color = random.randint(0,255)
    return 255, color, color

def Pallete03():
    return 0, random.randint(0, 255), 0

def Pallete04():
    color = random.randint(0, 255)
    return color, 255, color

def Pallete05():
    return 0, 0, random.randint(0, 255)

def Pallete06():
    color = random.randint(0, 255)
    return color, color, 255

def Pallete07():
    return random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)


def textObject(txt, font, color):
    text = font.render(txt, True, color)
    return text, text.get_rect()

MENUFONT = pygame.font.SysFont(None, 30)


### SOUNDS
MENUMUSIC = 'Audio/MenuMusic.wav'
GAMEMUSIC = 'Audio/GameMusic.wav'

VOLUME = 1.0


### IMAGES
MENUBG = pygame.image.load('Images/MainMenu.png')
GAMEBG = pygame.image.load('Images/background.png')

IMGSTARTOFF = pygame.image.load('Images/StartOff.png')
IMGSTARTON = pygame.image.load('Images/StartOn.png')
IMGSTARTPRESSED = pygame.image.load('Images/StartPressed.png')
IMGSTARTSIZE = IMGSTARTOFF.get_size()

IMGQUITOFF = pygame.image.load('Images/QuitOff.png')
IMGQUITON = pygame.image.load('Images/QuitOn.png')
IMGQUITPRESSED = pygame.image.load('Images/QuitPressed.png')
IMGQUITSIZE = IMGSTARTOFF.get_size()

IMGOPTIONSOFF = pygame.image.load('Images/OptionsOff.png')
IMGOPTIONSON = pygame.image.load('Images/OptionsOn.png')
IMGOPTIONSPRESSED = pygame.image.load('Images/OptionsPressed.png')
IMGOPTIONSSIZE = IMGOPTIONSOFF.get_size()

IMGMAINMENUOFF = pygame.image.load('Images/MainMenuOff.png')
IMGMAINMENUON = pygame.image.load('Images/MainMenuOn.png')
IMGMAINMENUPRESSED = pygame.image.load('Images/MainMenuPressed.png')
IMGMAINMENUSIZE = IMGMAINMENUOFF.get_size()

IMGVOLUMEOFF = pygame.image.load('Images/VolumeOff.png')
IMGVOLUMEON = pygame.image.load('Images/VolumeOn.png')
IMGVOLUMEPRESSED  = pygame.image.load('Images/VolumePressed.png')
IMGVOLUMESIZE = IMGVOLUMEOFF.get_size()

IMGVOLUMEUPOFF = pygame.image.load('Images/VolumeUpOff.png')
IMGVOLUMEUPON = pygame.image.load('Images/VolumeUpOn.png')
IMGVOLUMEUPPRESSED = pygame.image.load('Images/VolumeUpPressed.png')
IMGVOLUMEUPSIZE  = IMGVOLUMEUPOFF.get_size()

IMGVOLUMEDOWNOFF = pygame.image.load('Images/VolumeDownOff.png')
IMGVOLUMEDOWNON = pygame.image.load('Images/VolumeDownOn.png')
IMGVOLUMEDOWNPRESSED = pygame.image.load('Images/VolumeDownPressed.png')
IMGVOLUMEDOWNSIZE =  IMGVOLUMEDOWNOFF.get_size()

CURSORICON = pygame.image.load('Images/cursor.gif')
cursorRect = CURSORICON.get_rect()

PLAYERSHIP = pygame.image.load('Images/PlayerShip.jpg')
BULLET  = pygame.image.load('Images/Bullet.png')

#--This makes all the walls a constant--
WALL01 = pygame.image.load('Images/Walls/01.png')
WALL02 = pygame.image.load('Images/Walls/02.png')
WALL03 = pygame.image.load('Images/Walls/03.png')
WALL04 = pygame.image.load('Images/Walls/04.png')
WALL05 = pygame.image.load('Images/Walls/05.png')
WALL06 = pygame.image.load('Images/Walls/06.png')
WALL07 = pygame.image.load('Images/Walls/07.png')
WALL08 = pygame.image.load('Images/Walls/08.png')
WALL09 = pygame.image.load('Images/Walls/09.png')
WALL10 = pygame.image.load('Images/Walls/10.png')
WALL11 = pygame.image.load('Images/Walls/11.png')
WALL12 = pygame.image.load('Images/Walls/12.png')
WALL13 = pygame.image.load('Images/Walls/13.png')
WALL14 = pygame.image.load('Images/Walls/14.png')
WALL15 = pygame.image.load('Images/Walls/15.png')
WALL16 = pygame.image.load('Images/Walls/16.png')
WALL17 = pygame.image.load('Images/Walls/17.png')
WALL18 = pygame.image.load('Images/Walls/18.png')
WALL19 = pygame.image.load('Images/Walls/19.png')
WALL20 = pygame.image.load('Images/Walls/20.png')
WALL21 = pygame.image.load('Images/Walls/21.png')
WALL22 = pygame.image.load('Images/Walls/22.png')
WALL23 = pygame.image.load('Images/Walls/23.png')
WALL24 = pygame.image.load('Images/Walls/24.png')
WALL25 = pygame.image.load('Images/Walls/25.png')
WALL26 = pygame.image.load('Images/Walls/26.png')
WALL27 = pygame.image.load('Images/Walls/27.png')
WALL28 = pygame.image.load('Images/Walls/28.png')
WALL29 = pygame.image.load('Images/Walls/29.png')
WALL30 = pygame.image.load('Images/Walls/30.png')
WALL31 = pygame.image.load('Images/Walls/31.png')
WALL32 = pygame.image.load('Images/Walls/32.png')
WALL33 = pygame.image.load('Images/Walls/33.png')
WALL34 = pygame.image.load('Images/Walls/34.png')
WALL35 = pygame.image.load('Images/Walls/35.png')
WALL36 = pygame.image.load('Images/Walls/36.png')
WALL37 = pygame.image.load('Images/Walls/37.png')
WALL38 = pygame.image.load('Images/Walls/38.png')
WALL39 = pygame.image.load('Images/Walls/39.png')
WALL40 = pygame.image.load('Images/Walls/40.png')
WALL41 = pygame.image.load('Images/Walls/41.png')

#--This puts those constants in an easy to read list--
wallNum = [WALL01,
           WALL02,
           WALL03,
           WALL04,
           WALL05,
           WALL06,
           WALL07,
           WALL08,
           WALL09,
           WALL10,
           WALL11,
           WALL12,
           WALL13,
           WALL14,
           WALL15,
           WALL16,
           WALL17,
           WALL18,
           WALL19,
           WALL20,
           WALL21,
           WALL22,
           WALL23,
           WALL24,
           WALL25,
           WALL26,
           WALL27,
           WALL28,
           WALL29,
           WALL30,
           WALL31,
           WALL32,
           WALL33,
           WALL34,
           WALL35,
           WALL36,
           WALL37,
           WALL38,
           WALL39,
           WALL40,
           WALL41,
           ]

#--This places the walls on the map for collsion purposes--
allWalls = [WALL01.get_rect(topleft=(0,0)),
WALL02.get_rect(topleft=(192,0)),
WALL03.get_rect(topleft=(405,0)),
WALL04.get_rect(topleft=(780,0)),
WALL05.get_rect(topleft=(978,0)),
WALL06.get_rect(topleft=(1155,52)),
WALL07.get_rect(topleft=(0,159)),
WALL08.get_rect(topleft=(310,215)),
WALL09.get_rect(topleft=(842,216)),
WALL10.get_rect(topleft=(1212,118)),
WALL11.get_rect(topleft=(0,252)),
WALL12.get_rect(topleft=(193,258)),
WALL13.get_rect(topleft=(203,283)),
WALL14.get_rect(topleft=(263,282)),
WALL15.get_rect(topleft=(673,327)),
WALL16.get_rect(topleft=(815,329)),
WALL17.get_rect(topleft=(1152,250)),
WALL18.get_rect(topleft=(104,375)),
WALL19.get_rect(topleft=(259,374)),
WALL20.get_rect(topleft=(0,388)),
WALL21.get_rect(topleft=(300,428)),
WALL22.get_rect(topleft=(0,446)),
WALL23.get_rect(topleft=(337,444)),
WALL24.get_rect(topleft=(339,459)),
WALL25.get_rect(topleft=(913,500)),
WALL26.get_rect(topleft=(1053,480)),
WALL27.get_rect(topleft=(487,500)),
WALL28.get_rect(topleft=(292,522)),
WALL29.get_rect(topleft=(427,573)),
WALL30.get_rect(topleft=(587,497)),
WALL31.get_rect(topleft=(684,511)),
WALL32.get_rect(topleft=(810,605)),
WALL33.get_rect(topleft=(0,654)),
WALL34.get_rect(topleft=(63,705)),
WALL35.get_rect(topleft=(654,683)),
WALL36.get_rect(topleft=(805,605)),
WALL37.get_rect(topleft=(586,140)),
WALL38.get_rect(topleft=(611,172)),
WALL39.get_rect(topleft=(739,305)),
WALL40.get_rect(topleft=(753,265)),
WALL41.get_rect(topleft=(789,245)),
            ]
		



### SPRITES AND GROUPS
playerGroup = pygame.sprite.Group()
bulletGroup = pygame.sprite.Group()
particleGroup = pygame.sprite.Group()

class Ship(pygame.sprite.Sprite):
    """Our ship"""
    image = PLAYERSHIP
    image.set_colorkey((0,0,0))
    image = image.convert_alpha()

    def __init__(self):
        self.groups = playerGroup
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.area = GAMEWINDOW.get_rect()
        self.boost = False
        if len(joysticks) >= 1:
            self.joy = True
        else:
            self.joy = False
        self.reverse = False
        self.dx = 0
        self.dy = 0
        self.canShoot = False
        self.coolDown = 0
        self.image = Ship.image
        self.palette = random.randint(0,5)
        self.rect = self.image.get_rect()
        self.radius = self.image.get_width() / 2.0
        self.speed = 5
        self.angle = 0
        self.rotateSpeed = 3.14
        self.shots = 0

    def kill(self):
        pygame.sprite.Sprite.kill(self)

    def speedCheck(self):
        if abs(self.dx) > 0:
            self.dx *= FRICTION
        if abs(self.dy) > 0:
            self.dy *= FRICTION

    def areaCheck(self):
        if not self.area.contains(self.rect):
            if self.pos[0] + self.rect.width/2 > self.area.right:
                self.pos[0] = self.area.right - self.rect.width/2
                #self.dx *= 0.5
            if self.pos[0] - self.rect.width/2 < self.area.left:
                self.pos[0] = self.area.left + self.rect.width/2
                #self.dx *= 0.5
            if self.pos[1] + self.rect.height/2 > self.area.bottom:
                self.pos[1] = self.area.bottom - self.rect.height/2
                #self.dx *= 0.5
            if self.pos[1] - self.rect.height/2 < self.area.top:
                self.pos[1] = self.area.top + self.rect.height/2
                #self.dx *= 0.5

    def shoot(self):
        #print("SHOOT")
        bullet = Bullet([self.pos[0], self.pos[1]], self.angle)

    def updatePre(self, time):
        self.ddx = 0.0
        self.ddy = 0.0
        if self.boost:
            self.newRotateSpeed = self.rotateSpeed * 0.3
        else:
            self.newRotateSpeed = self.rotateSpeed

    def updatePost(self, time):
        # ---Shoot Cooldown---
        if self.canShoot:
            self.shoot()
            self.canShoot = not self.canShoot
        elif not self.canShoot and 0 < self.coolDown < 60:
            self.coolDown += 1
        else:
            self.coolDown = 0
        #---Movement---
        if self.boost and not self.reverse:
            self.dx += self.ddx * self.speed * 1.5
            self.dy += self.ddy * self.speed * 1.5
        elif self.reverse:
            self.dx += self.ddx * self.speed * 0.5
            self.dy += self.ddy * self.speed * 0.5
        else:
            self.dx += self.ddx * self.speed
            self.dy += self.ddy * self.speed
        self.speedCheck()
        self.pos[0] += self.dx * time
        self.pos[1] += self.dy * time
        self.areaCheck()
        #---Draw Object---
        self.oldCenter = self.rect.center
        self.newImage = pygame.transform.rotate(self.image, self.angle)
        self.rect = self.newImage.get_rect()
        self.rect.center = self.oldCenter
        self.rect.centerx = round(self.pos[0], 0)
        self.rect.centery = round(self.pos[1], 0)
        GAMEWINDOW.blit(self.newImage, self.rect)

class Player1(Ship):
    """Player1 Class"""
    def __init__(self):
        Ship.__init__(self)
        self.pos = [GWX, GWY+20]

    def update(self, time):
        Ship.updatePre(self, time)
        # ---Input Player 1---
        pressedKeys = pygame.key.get_pressed()
        if pressedKeys[pygame.K_w]:
            self.ddx = -math.sin(self.angle * RAD)
            self.ddy = -math.cos(self.angle * RAD)
            CarFX(self.rect.center, -self.ddx, -self.ddy, self.palette)
            if pressedKeys[pygame.K_LSHIFT]:
                self.boost = True
                BoostFX(self.rect.center, -self.ddx, -self.ddy)
            else:
                self.boost = False
        if pressedKeys[pygame.K_SPACE] and self.coolDown == 0:
            self.canShoot = True
            self.coolDown = 1
        if pressedKeys[pygame.K_s]:
            self.ddx = +math.sin(self.angle * RAD)
            self.ddy = +math.cos(self.angle * RAD)
            self.reverse = True
        else:
            self.reverse = False
        if pressedKeys[pygame.K_a]:
            self.angle += self.newRotateSpeed
        if pressedKeys[pygame.K_d]:
            self.angle -= self.newRotateSpeed
        #---Joystick Input---
        if self.joy:
            if joysticks[0].get_button(0) and self.coolDown == 0:
                self.canShoot = True
                self.coolDown = 1
            if joysticks[0].get_axis(0) > 0.3:
                self.angle -= self.newRotateSpeed
            if joysticks[0].get_axis(0) < -0.3:
                self.angle += self.newRotateSpeed
            if joysticks[0].get_axis(2) < -0.3:
                self.ddx = -math.sin(self.angle * RAD)
                self.ddy = -math.cos(self.angle * RAD)
                CarFX(self.rect.center, -self.ddx, -self.ddy, self.palette)
                if joysticks[0].get_button(2):
                    self.boost = True
                    BoostFX(self.rect.center, -self.ddx, -self.ddy)
                else:
                    self.boost = False
            if joysticks[0].get_axis(2) > 0.3:
                self.ddx = +math.sin(self.angle * RAD)
                self.ddy = +math.cos(self.angle * RAD)
                self.reverse = True
            else:
                self.reverse = False
        #---End Input---
        Ship.updatePost(self, time)

class Player2(Ship):
    """Player1 Class"""
    def __init__(self):
        Ship.__init__(self)
        self.pos = [GWX, GWY-20]

    def update(self, time):
        Ship.updatePre(self, time)
        # ---Input Player 1---
        pressedKeys = pygame.key.get_pressed()
        if pressedKeys[pygame.K_UP]:
            self.ddx = -math.sin(self.angle * RAD)
            self.ddy = -math.cos(self.angle * RAD)
            CarFX(self.rect.center, -self.ddx, -self.ddy, self.palette)
            if pressedKeys[pygame.K_KP_ENTER]:
                self.boost = True
                BoostFX(self.rect.center, -self.ddx, -self.ddy)
            else:
                self.boost = False
        if pressedKeys[pygame.K_KP0] and self.coolDown == 0:
            self.canShoot = True
            self.coolDown = 1
        if pressedKeys[pygame.K_DOWN]:
            self.ddx = +math.sin(self.angle * RAD)
            self.ddy = +math.cos(self.angle * RAD)
            self.reverse = True
        else:
            self.reverse = False
        if pressedKeys[pygame.K_LEFT]:
            self.angle += self.newRotateSpeed
        if pressedKeys[pygame.K_RIGHT]:
            self.angle -= self.newRotateSpeed
        #---Joystick Input---
        if self.joy:
            if joysticks[1].get_button(0) and self.coolDown == 0:
                self.canShoot = True
                self.coolDown = 1
            if joysticks[1].get_axis(0) > 0.3:
                self.angle -= self.newRotateSpeed
            if joysticks[1].get_axis(0) < -0.3:
                self.angle += self.newRotateSpeed
            if joysticks[1].get_axis(2) < -0.3:
                self.ddx = -math.sin(self.angle * RAD)
                self.ddy = -math.cos(self.angle * RAD)
                CarFX(self.rect.center, -self.ddx, -self.ddy, self.palette)
                if joysticks[1].get_button(2):
                    self.boost = True
                    BoostFX(self.rect.center, -self.ddx, -self.ddy)
                else:
                    self.boost = False
            if joysticks[1].get_axis(2) > 0.3:
                self.ddx = +math.sin(self.angle * RAD)
                self.ddy = +math.cos(self.angle * RAD)
                self.reverse = True
            else:
                self.reverse = False
        #---End Input---
        Ship.updatePost(self, time)

class Bullet(pygame.sprite.Sprite):
    """What we shoot"""
    image = BULLET
    image.set_colorkey((0,0,0))
    image = image.convert_alpha()

    def __init__(self, startpos, ang):
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.pos = startpos
        self.angle = ang
        self.image = Bullet.image
        self.rect = self.image.get_rect()
        self.dx = 0
        self.dy = 0
        self.speed = 6
        self.counter = 0

    def update(self):
        self.ddx = 0.0
        self.ddy = 0.0
        self.ddx = -math.sin(self.angle * RAD)
        self.ddy = -math.cos(self.angle * RAD)
        BulletFX(self.rect.center, -self.ddx, -self.ddy)
        if self.counter < 60:
            #print(self.counter)
            self.dx = self.ddx * self.speed
            self.dy = self.ddy * self.speed
            self.pos[0] += self.dx
            self.pos[1] += self.dy
            self.rect.centerx = round(self.pos[0], 0)
            self.rect.centery = round(self.pos[1], 0)
            self.counter += 1
        else:
            #print("BULLET KILL")
            pygame.sprite.Sprite.kill(self)
        GAMEWINDOW.blit(self.image, self.rect)

class Particles(pygame.sprite.Sprite):
    """Base class for particles .... bro"""
    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.pos = [0.0, 0.0]
        self.partMaxSpeed = 5

    def init2(self):
        self.image = pygame.Surface((10, 10))
        self.image.set_colorkey((0, 0, 0))
        pygame.draw.circle(self.image, self.color, (5, 5), random.randint(1,4))
        self.image = self.image.convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.center = self.pos
        self.time = 0.0

    def update(self, seconds):
        self.time += seconds
        if self.time > self.lifetime:
            self.kill()
        self.pos[0] += self.dx * seconds * self.partMaxSpeed
        self.pos[1] += self.dy * seconds * self.partMaxSpeed
        self.rect.centerx = round(self.pos[0], 0)
        self.rect.centery = round(self.pos[1], 0)

class CarFX(Particles):
    """Serious Spark FX"""
    def __init__(self, pos, dx, dy, col):
        if col == 0:
            self.color = Pallete01()
        elif col == 1:
            self.color = Pallete02()
        elif col == 2:
            self.color = Pallete03()
        elif col == 3:
            self.color = Pallete04()
        elif col == 4:
            self.color = Pallete05()
        elif col == 5:
            self.color = Pallete06()
        else:
            self.color = Pallete07()
        self.groups = particleGroup
        self.pos = pos
        Particles.__init__(self, self.pos)
        Particles.init2(self)
        self.pos[0] = pos[0]
        self.pos[1] = pos[1]
        self.lifetime= 0 + random.random()*1.5
        self.partSpeed = 10
        self.partArc = 0.3
        arc = self.partSpeed * self.partArc
        self.dx = dx * self.partSpeed + random.random()*2*arc - arc
        self.dy = dy * self.partSpeed + random.random()*2*arc - arc

class BulletFX(Particles):
    """Moar Serious Spark FX"""
    def __init__(self, pos, dx, dy):
        self.col = random.randint(100, 255)
        self.color = (self.col, self.col, 0)
        self.groups = particleGroup
        self.pos = pos
        Particles.__init__(self, self.pos)
        Particles.init2(self)
        self.pos[0] = pos[0]
        self.pos[1] = pos[1]
        self.lifetime= 0 + random.random()*0.1
        self.partSpeed = 50
        self.partArc = 1
        arc = self.partSpeed * self.partArc
        self.dx = dx * self.partSpeed + random.random()*2*arc - arc
        self.dy = dy * self.partSpeed + random.random()*2*arc - arc

class BoostFX(Particles):
    """Moar Serious Spark FX for BOOST"""
    def __init__(self, pos, dx, dy):
        self.color = Pallete07()
        self.groups = particleGroup
        self.pos = pos
        Particles.__init__(self, self.pos)
        Particles.init2(self)
        self.pos[0] = pos[0]
        self.pos[1] = pos[1]
        self.lifetime = 0 + random.random() * 0.5
        self.partSpeed = 100
        self.partArc = 0.3
        arc = self.partSpeed * self.partArc
        self.dx = dx * self.partSpeed + random.random() * 2 * arc - arc
        self.dy = dy * self.partSpeed + random.random() * 2 * arc - arc

Ship.groups = playerGroup
Bullet.groups = bulletGroup
Particles.groups = particleGroup


### BUTTONS
class Button():
    """Button Class"""
    def __init__(self, imgOff, imgOn, imgPressed, cent=(0,0)):
        self.imageOff = imgOff
        self.imageOn = imgOn
        self.imagePressed = imgPressed
        self.size = imgOff.get_size()
        self.rect = imgOff.get_rect()
        #self.rect = pygame.Rect(left - self.size[0], top - self.size[1], self.size[0], self.size[1])
        self.rect.center = cent

    def update(self, curs, click):
        cursorPos = curs

        if (self.rect.left < cursorPos[0] < self.rect.right and
            self.rect.top < cursorPos[1] < self.rect.bottom):
            GAMEWINDOW.blit(self.imageOn, self.rect)
            if click == 1:
                GAMEWINDOW.blit(self.imagePressed, self.rect)
                return True
        else:
            GAMEWINDOW.blit(self.imageOff, self.rect)


### FUNCTIONS
def main():
    """Main game engine."""
    global GAMEWINDOW, MAINCLOCK, MIXER, MENUBG, VOLUME

    buttonStart = Button(IMGSTARTOFF, IMGSTARTON, IMGSTARTPRESSED, (GWX * 0.66, GWY - 50))
    buttonQuit = Button(IMGQUITOFF, IMGQUITON, IMGQUITPRESSED, (GWX * 0.66, GWY + 75))
    buttonVolume = Button(IMGVOLUMEOFF, IMGVOLUMEOFF, IMGVOLUMEOFF, (GWX * 0.66, GWY + 200))
    buttonVolumeUp = Button(IMGVOLUMEUPOFF, IMGVOLUMEUPON, IMGVOLUMEUPPRESSED, (GWX * 0.82, GWY + 200))
    buttonVoluemDown = Button(IMGVOLUMEDOWNOFF, IMGVOLUMEDOWNON, IMGVOLUMEDOWNPRESSED, (GWX * 0.5, GWY + 200))

    while True:
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos

        MIXER.set_volume(VOLUME)
        if not MIXER.get_busy():
            MIXER.load(MENUMUSIC)
            MIXER.play(-1)

        # SCREEN REFRESH
        GAMEWINDOW.blit(MENUBG, (0,0))

        if buttonStart.update(mousePos, click1):
            if pygame.event.get(MOUSEBUTTONUP):
                shooty()
        if buttonQuit.update(mousePos, click1):
            if pygame.event.get(MOUSEBUTTONUP):
                quitGame()
        buttonVolume.update(mousePos, click1)
        if buttonVoluemDown.update(mousePos, click1) and VOLUME > 0.0:
            if pygame.event.get(MOUSEBUTTONUP):
                VOLUME -= 0.1
                print(VOLUME)
        if buttonVolumeUp.update(mousePos, click1) and VOLUME < 1.0:
            if pygame.event.get(MOUSEBUTTONUP):
                VOLUME += 0.1
                print(VOLUME)

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    quitGame()

        GAMEWINDOW.blit(CURSORICON, mousePos)
        pygame.display.update()
        MAINCLOCK.tick(60)

def inGameMenu(cursorPos, click):
    """Menu overlay during game."""
    buttonMenuQuit = Button(IMGQUITOFF, IMGQUITON, IMGQUITPRESSED, (GWX, GWY+30))
    buttonMenuReturn = Button(IMGMAINMENUOFF, IMGMAINMENUON, IMGMAINMENUPRESSED, (GWX, GWY-30))
    menuWindow = pygame.Surface((500, 250))
    menuWindow.set_alpha(128)
    menuWindow.fill((40, 40, 50))
    GAMEWINDOW.blit(menuWindow, (GWX - 250, GWY - 125))

    if buttonMenuQuit.update(cursorPos, click):
        pygame.time.delay(100)
        if pygame.event.get(MOUSEBUTTONUP):
            return 0
    if buttonMenuReturn.update(cursorPos, click):
        pygame.time.delay(100)
        if pygame.event.get(MOUSEBUTTONUP):
            return 1

def startGame():
    """Run actual game here. !! NOT USED YET !!"""
    global GAMEWINDOW, MAINCLOCK, MIXER

    tempText, tempRect = textObject("THE CAKE IS A LIE", MENUFONT, BLACK)
    temp2Text, temp2Rect = textObject("THE LIE IS A CAKE", MENUFONT, RED)
    tempRect.center = GWC
    temp2Rect.center = GWC

    flasher = 0
    menuOn = False
    MIXER.stop()


def shooty():
    """Shooter Prototype."""
    global GAMEWINDOW, MAINCLOCK, MIXER, VOLUME

    MIXER.stop()
    MIXER.load(GAMEMUSIC)

    menuOn = False
    player1 = Player1()
    player2 = Player2()
    seconds = MAINCLOCK.tick(60) / 1000
    joyCount = 30

    while True:
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos
        print(seconds)
        MIXER.set_volume(VOLUME)
        if not MIXER.get_busy():
            MIXER.play(-1)

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    menuOn = not menuOn

        #---Joystick---
        for joy in joysticks:
            if joy.get_button(7) and joyCount == 30:
                menuOn = not menuOn
                joyCount -= 1
            elif 0 < joyCount < 30:
                joyCount -= 1
            else:
                joyCount = 30

        GAMEWINDOW.fill(GREY)
        GAMEWINDOW.blit(GAMEBG, (0,0))

        if menuOn:
            if inGameMenu(mousePos, click1) == 0:
                quitGame()
            elif inGameMenu(mousePos, click1) == 1:
                # ---Remove game objects from scene before returning to main menu.
                playerGroup.empty()
                bulletGroup.empty()
                MIXER.stop()
                return None
            GAMEWINDOW.blit(CURSORICON, mousePos)

        if not menuOn:
            playerGroup.update(seconds)
            bulletGroup.update()
            particleGroup.update(seconds)
            particleGroup.draw(GAMEWINDOW)

        #Testing to see if the game is over or if someone is killed to death
        #player = Ship()
        #if TIMELIMIT > 18000 or player1Health = 0 or player2Health = 0
            #call kill function
            #pause the game
            #ask if they want to play again
        
        pygame.display.update()
        MAINCLOCK.tick(60)

        

def quitGame():
    """Exit pygame and system."""
    pygame.quit()
    sys.exit()

### Run Game
if __name__ == "__main__":
    main()
