KEYBOARD CONTROLS

Player 1

W � Accelerate
S � Brake / Reverse
A � Turn Left
D � Turn Right
LSHIFT � Boost
SPACE � Shoot

Player 2

UP � Accelerate
DOWN � Brake / Reverse
LEFT � Turn Left
RIGHT � Turn Right
RShift � Boost
RETURN � Shoot

Xbox 360 Controller (P1 and P2)

(LS) � Steer

RT � Accelerate
LT � Brake / Reverse
A � Shoot
B � Drift
X � Shoot
