"""
jrWIP.py
John Ryan's Prototype module for Group Awesome's COP Final Project.
This prototype focuses on the GUI elements for the project.
"""

import sys
import pygame
import math
import random
from pygame.locals import *


### PYGAME SETUP
pygame.init()
pygame.mouse.set_visible(False)
pygame.mixer.init(44100, -16, 2, 2048)
MIXER = pygame.mixer.music
MAINCLOCK = pygame.time.Clock()
#---Joysticks---
joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
for joy in joysticks:
    joy.init()
if len(joysticks) == 2:
    joysticks.reverse()

### SCREEN SETUP
pygame.display.set_caption("JR WIP")

SCREEN_INFO = pygame.display.Info()
WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

GAMEWINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 0)

GWX = GAMEWINDOW.get_rect().centerx
GWY = GAMEWINDOW.get_rect().centery
GWC = GAMEWINDOW.get_rect().center
GWL = GAMEWINDOW.get_rect().left
GWR = GAMEWINDOW.get_rect().right
GWT = GAMEWINDOW.get_rect().top
GWB = GAMEWINDOW.get_rect().bottom

### MATH
FRICTION = 0.97
RAD = math.pi / 180


### COLORS AND FONTS
BLACK = (0, 0, 0)
GREY = (25, 25, 25)
RED = (255, 0, 0)
DARK_RED = (155, 0, 0)
GREEN = (0, 255, 0)
DARK_GREEN = (0, 155, 0)
BLUE = (0, 0, 255)
PURPLE = (155, 0, 200)
WHITE = (255, 255, 255)

def Pallete01():
    """Dark Reds"""
    return random.randint(0, 255), 0, 0

def Pallete02():
    """Light Reds"""
    color = random.randint(0,255)
    return 255, color, color

def Pallete03():
    """Dark Greens"""
    return 0, random.randint(0, 255), 0

def Pallete04():
    """Light Greens"""
    color = random.randint(0, 255)
    return color, 255, color

def Pallete05():
    """Dark Blues"""
    return 0, 0, random.randint(0, 255)

def Pallete06():
    """Light Blues"""
    color = random.randint(0, 255)
    return color, color, 255

def Pallete07():
    """Rainbow"""
    return random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)

def Pallete08():
    """Yellows"""
    color = random.randint(0, 255)
    return color, color, 0

def Pallete09():
    """Bright Yellows"""
    color = random.randint(200, 255)
    return color, color, 255

def textObject(txt, font, color):
    text = font.render(txt, True, color)
    return text, text.get_rect()

MENUFONT = pygame.font.SysFont(None, 30)
COUNTFONT = pygame.font.SysFont(None, 72)


### SOUNDS
MENUMUSIC = 'Audio/MenuMusic.wav'
GAMEMUSIC = 'Audio/GameMusic.wav'

VOLUME = 1.0


### IMAGES
MENUBG = pygame.image.load('Images/MainMenu.png').convert_alpha()
GAMEBG = pygame.image.load('Images/background.png').convert_alpha()

IMGSTARTOFF = pygame.image.load('Images/StartOff.png').convert_alpha()
IMGSTARTON = pygame.image.load('Images/StartOn.png').convert_alpha()
IMGSTARTPRESSED = pygame.image.load('Images/StartPressed.png').convert_alpha()
IMGSTARTSIZE = IMGSTARTOFF.get_size()

IMGQUITOFF = pygame.image.load('Images/QuitOff.png').convert_alpha()
IMGQUITON = pygame.image.load('Images/QuitOn.png').convert_alpha()
IMGQUITPRESSED = pygame.image.load('Images/QuitPressed.png').convert_alpha()
IMGQUITSIZE = IMGSTARTOFF.get_size()

IMGOPTIONSOFF = pygame.image.load('Images/OptionsOff.png').convert_alpha()
IMGOPTIONSON = pygame.image.load('Images/OptionsOn.png').convert_alpha()
IMGOPTIONSPRESSED = pygame.image.load('Images/OptionsPressed.png').convert_alpha()
IMGOPTIONSSIZE = IMGOPTIONSOFF.get_size()

IMGMAINMENUOFF = pygame.image.load('Images/MainMenuOff.png').convert_alpha()
IMGMAINMENUON = pygame.image.load('Images/MainMenuOn.png').convert_alpha()
IMGMAINMENUPRESSED = pygame.image.load('Images/MainMenuPressed.png').convert_alpha()
IMGMAINMENUSIZE = IMGMAINMENUOFF.get_size()

IMGVOLUMEOFF = pygame.image.load('Images/VolumeOff.png').convert_alpha()
IMGVOLUMEON = pygame.image.load('Images/VolumeOn.png').convert_alpha()
IMGVOLUMEPRESSED  = pygame.image.load('Images/VolumePressed.png').convert_alpha()
IMGVOLUMESIZE = IMGVOLUMEOFF.get_size()

IMGVOLUMEUPOFF = pygame.image.load('Images/VolumeUpOff.png').convert_alpha()
IMGVOLUMEUPON = pygame.image.load('Images/VolumeUpOn.png').convert_alpha()
IMGVOLUMEUPPRESSED = pygame.image.load('Images/VolumeUpPressed.png').convert_alpha()
IMGVOLUMEUPSIZE  = IMGVOLUMEUPOFF.get_size()

IMGVOLUMEDOWNOFF = pygame.image.load('Images/VolumeDownOff.png').convert_alpha()
IMGVOLUMEDOWNON = pygame.image.load('Images/VolumeDownOn.png').convert_alpha()
IMGVOLUMEDOWNPRESSED = pygame.image.load('Images/VolumeDownPressed.png').convert_alpha()
IMGVOLUMEDOWNSIZE =  IMGVOLUMEDOWNOFF.get_size()

CURSORICON = pygame.image.load('Images/cursor.gif').convert_alpha()
cursorRect = CURSORICON.get_rect()

BULLET  = pygame.image.load('Images/Bullet.png').convert_alpha()

#---load Car models---
TANK = pygame.image.load('Images/Cars/Tank.png').convert_alpha()
FLOAT = pygame.image.load('Images/Cars/Float.png').convert_alpha()
DELOREAN = pygame.image.load('Images/Cars/Delorean.png').convert_alpha()
BATCAR = pygame.image.load('Images/Cars/Batcar.png').convert_alpha()

CARLIST = [TANK,
           FLOAT,
           DELOREAN,
           BATCAR,]

player1Car = random.choice(CARLIST)
player2Car = random.choice(CARLIST)
while player1Car == player2Car:
    player2Car = random.choice(CARLIST)

#---Load Wall Images---
WALL01 = pygame.image.load('Images/Walls/01.png').convert_alpha()
WALL02 = pygame.image.load('Images/Walls/02.png').convert_alpha()
WALL03 = pygame.image.load('Images/Walls/03.png').convert_alpha()
WALL04 = pygame.image.load('Images/Walls/04.png').convert_alpha()
WALL05 = pygame.image.load('Images/Walls/05.png').convert_alpha()
WALL06 = pygame.image.load('Images/Walls/06.png').convert_alpha()
WALL07 = pygame.image.load('Images/Walls/07.png').convert_alpha()
WALL08 = pygame.image.load('Images/Walls/08.png').convert_alpha()
WALL09 = pygame.image.load('Images/Walls/09.png').convert_alpha()
WALL10 = pygame.image.load('Images/Walls/10.png').convert_alpha()
WALL11 = pygame.image.load('Images/Walls/11.png').convert_alpha()
WALL12 = pygame.image.load('Images/Walls/12.png').convert_alpha()
WALL13 = pygame.image.load('Images/Walls/13.png').convert_alpha()
WALL14 = pygame.image.load('Images/Walls/14.png').convert_alpha()
WALL15 = pygame.image.load('Images/Walls/15.png').convert_alpha()
WALL16 = pygame.image.load('Images/Walls/16.png').convert_alpha()
WALL17 = pygame.image.load('Images/Walls/17.png').convert_alpha()
WALL18 = pygame.image.load('Images/Walls/18.png').convert_alpha()
WALL19 = pygame.image.load('Images/Walls/19.png').convert_alpha()
WALL20 = pygame.image.load('Images/Walls/20.png').convert_alpha()
WALL21 = pygame.image.load('Images/Walls/21.png').convert_alpha()
WALL22 = pygame.image.load('Images/Walls/22.png').convert_alpha()
WALL23 = pygame.image.load('Images/Walls/23.png').convert_alpha()
WALL24 = pygame.image.load('Images/Walls/24.png').convert_alpha()
WALL25 = pygame.image.load('Images/Walls/25.png').convert_alpha()
WALL26 = pygame.image.load('Images/Walls/26.png').convert_alpha()
WALL27 = pygame.image.load('Images/Walls/27.png').convert_alpha()
WALL28 = pygame.image.load('Images/Walls/28.png').convert_alpha()
WALL29 = pygame.image.load('Images/Walls/29.png').convert_alpha()
WALL30 = pygame.image.load('Images/Walls/30.png').convert_alpha()
WALL31 = pygame.image.load('Images/Walls/31.png').convert_alpha()
WALL32 = pygame.image.load('Images/Walls/32.png').convert_alpha()
WALL33 = pygame.image.load('Images/Walls/33.png').convert_alpha()
WALL34 = pygame.image.load('Images/Walls/34.png').convert_alpha()
WALL35 = pygame.image.load('Images/Walls/35.png').convert_alpha()
WALL36 = pygame.image.load('Images/Walls/36.png').convert_alpha()
WALL37 = pygame.image.load('Images/Walls/37.png').convert_alpha()

#---List of all Walls---
wallImages = [WALL01,
           WALL02,
           WALL03,
           WALL04,
           WALL05,
           WALL06,
           WALL07,
           WALL08,
           WALL09,
           WALL10,
           WALL11,
           WALL12,
           WALL13,
           WALL14,
           WALL15,
           WALL16,
           WALL17,
           WALL18,
           WALL19,
           WALL20,
           WALL21,
           WALL22,
           WALL23,
           WALL24,
           WALL25,
           WALL26,
           WALL27,
           WALL28,
           WALL29,
           WALL30,
           WALL31,
           WALL32,
           WALL33,
           WALL34,
           WALL35,
           WALL36,
           WALL37]

#---Set Wall rects---
wallRects = [WALL01.get_rect(topleft=(0,0)),
            WALL02.get_rect(topleft=(194,0)),
            WALL03.get_rect(topleft=(415,0)),
            WALL04.get_rect(topleft=(761,0)),
            WALL05.get_rect(topleft=(966,0)),
            WALL06.get_rect(topleft=(1167,0)),
            WALL07.get_rect(topleft=(1224,169)),
            WALL08.get_rect(topleft=(1163,262)),
            WALL09.get_rect(topleft=(1064,498)),
            WALL10.get_rect(topleft=(926,525)),
            WALL11.get_rect(topleft=(820,616)),
            WALL12.get_rect(topleft=(663,694)),
            WALL13.get_rect(topleft=(428,709)),
            WALL14.get_rect(topleft=(0,647)),
            WALL15.get_rect(topleft=(0,438)),
            WALL16.get_rect(topleft=(0,263)),
            WALL17.get_rect(topleft=(0,148)),
            WALL18.get_rect(topleft=(107,389)),
            WALL19.get_rect(topleft=(107,418)),
            WALL20.get_rect(topleft=(324,228)),
            WALL21.get_rect(topleft=(273,292)),
            WALL22.get_rect(topleft=(205,292)),
            WALL23.get_rect(topleft=(350,418)),
            WALL24.get_rect(topleft=(304,418)),
            WALL25.get_rect(topleft=(477,418)),
            WALL26.get_rect(topleft=(332,531)),
            WALL27.get_rect(topleft=(306,547)),
            WALL28.get_rect(topleft=(477,523)),
            WALL29.get_rect(topleft=(440,563)),
            WALL30.get_rect(topleft=(477,511)),
            WALL31.get_rect(topleft=(686,339)),
            WALL32.get_rect(topleft=(855,228)),
            WALL33.get_rect(topleft=(818,338)),
            WALL34.get_rect(topleft=(842,338)),
            WALL35.get_rect(topleft=(609,513)),
            WALL36.get_rect(topleft=(768,270)),
            WALL37.get_rect(topleft=(597,146))]


### SPRITES AND GROUPS
playerGroup = pygame.sprite.Group()
bulletGroup = pygame.sprite.Group()
particleGroup = pygame.sprite.Group()

class Car(pygame.sprite.Sprite):
    """Our ship"""
    def __init__(self):
        self.health = 5
        self.groups = playerGroup
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.area = GAMEWINDOW.get_rect()
        self.boost = False
        self.reverse = False
        self.dx = 0
        self.dy = 0
        self.ddx = 0.0
        self.ddy = 0.0
        self.canShoot = False
        self.coolDown = 0
        self.rect = self.image.get_rect()
        self.sliding = False
        self.slideFriction = 0.99
        self.speed = 5
        self.angle = 0
        self.rotateSpeed = 4
        self.shots = 0

    def kill(self):
        for i in range(1,30):
            KillFX(self.pos, 0, 0)
        pygame.sprite.Sprite.kill(self)

    def speedCheck(self):
        if self.sliding:
            if abs(self.dx) > 0:
                self.dx *= self.slideFriction
            if abs(self.dy) > 0:
                self.dy *= self.slideFriction
        else:
            if abs(self.dx) > 0:
                self.dx *= FRICTION
            if abs(self.dy) > 0:
                self.dy *= FRICTION

    def areaCheck(self):
        if not self.area.contains(self.rect):
            if self.pos[0] + self.rect.width/2 > self.area.right:
                self.pos[0] = self.area.right - self.rect.width/2
                #self.dx *= 0.5
            if self.pos[0] - self.rect.width/2 < self.area.left:
                self.pos[0] = self.area.left + self.rect.width/2
                #self.dx *= 0.5
            if self.pos[1] + self.rect.height/2 > self.area.bottom:
                self.pos[1] = self.area.bottom - self.rect.height/2
                #self.dx *= 0.5
            if self.pos[1] - self.rect.height/2 < self.area.top:
                self.pos[1] = self.area.top + self.rect.height/2
                #self.dx *= 0.5

    def shoot(self):
        bullet = Bullet([self.pos[0], self.pos[1]], self.angle, self.tag)

    def update(self, time):
        if self.boost:
            self.newRotateSpeed = self.rotateSpeed * 0.3
        else:
            self.newRotateSpeed = self.rotateSpeed
        # ---Shoot Cooldown---
        if self.canShoot:
            self.shoot()
            self.canShoot = not self.canShoot
        elif not self.canShoot and 0 < self.coolDown < 60:
            self.coolDown += 1
        else:
            self.coolDown = 0
        #---Movement---
        if self.boost and not self.reverse:
            self.dx += self.ddx * self.speed * 1.5
            self.dy += self.ddy * self.speed * 1.5
        elif self.reverse:
            self.dx += self.ddx * self.speed * 0.5
            self.dy += self.ddy * self.speed * 0.5
        else:
            self.dx += self.ddx * self.speed
            self.dy += self.ddy * self.speed
        self.speedCheck()
        self.pos[0] += self.dx * time
        self.pos[1] += self.dy * time
        self.areaCheck()
        #---Draw Object---
        self.oldCenter = self.rect.center
        self.newImage = pygame.transform.rotate(self.image, self.angle)
        self.rect = self.newImage.get_rect()
        self.rect.center = self.oldCenter
        self.rect.centerx = round(self.pos[0], 0)
        self.rect.centery = round(self.pos[1], 0)
        self.rectDetect = Rect(self.rect[0], self.rect[1],
                               self.rect[2] - 30, self.rect[3] - 30)
        self.rectDetect.center = self.rect.center
        GAMEWINDOW.blit(self.newImage, self.rect)

class Player1(Car):
    """Player1 Class"""
    image = player1Car

    def __init__(self):
        Car.__init__(self)
        if len(joysticks) > 0:
            self.joy = True
        else:
            self.joy = False
        self.pos = [200, 600]
        self.tag = "P1"
        self.image = Player1.image
        self.palette = random.randint(0,1)

    def update(self, time):
        self.ddx = 0.0
        self.ddy = 0.0
        # ---Input Player 1---
        if not self.joy:
            pressedKeys = pygame.key.get_pressed()
            if pressedKeys[pygame.K_SPACE] and self.coolDown == 0:
                self.canShoot = True
                self.coolDown = 1
            if pressedKeys[pygame.K_w]:
                self.ddx = -math.sin(self.angle * RAD)
                self.ddy = -math.cos(self.angle * RAD)
                CarFX(self.rect.center, -self.ddx, -self.ddy, self.palette)
                if pressedKeys[pygame.K_LSHIFT]:
                    self.boost = True
                    BoostFX(self.rect.center, -self.ddx, -self.ddy)
                if pressedKeys[pygame.K_s]:
                    self.sliding = True
                else:
                    self.sliding = False
            if not pressedKeys[pygame.K_LSHIFT]:
                self.boost = False
            if pressedKeys[pygame.K_s]:
                self.ddx = +math.sin(self.angle * RAD)
                self.ddy = +math.cos(self.angle * RAD)
                self.reverse = True
            else:
                self.reverse = False
            if pressedKeys[pygame.K_a]:
                self.angle += self.newRotateSpeed
            if pressedKeys[pygame.K_d]:
                self.angle -= self.newRotateSpeed
        #---Joystick Input---
        if self.joy:
            if joysticks[0].get_button(0) and self.coolDown == 0:
                self.canShoot = True
                self.coolDown = 1
            if joysticks[0].get_axis(0) > 0.3:
                self.angle -= self.newRotateSpeed
            if joysticks[0].get_axis(0) < -0.3:
                self.angle += self.newRotateSpeed
            if joysticks[0].get_axis(4) > 0.3:
                self.angle -= self.newRotateSpeed * 0.3
            if joysticks[0].get_axis(4) < -0.3:
                self.angle += self.newRotateSpeed * 0.3
            #---Accelerate and Reverse---
            if joysticks[0].get_axis(2) < -0.3:
                if joysticks[0].get_button(1):
                    self.sliding = True
                else:
                    self.sliding = False
                    self.ddx = -math.sin(self.angle * RAD)
                    self.ddy = -math.cos(self.angle * RAD)
                    CarFX(self.rect.center, -self.ddx, -self.ddy, self.palette)
                    if joysticks[0].get_button(2):
                        self.boost = True
                        BoostFX(self.rect.center, -self.ddx, -self.ddy)
            if not joysticks[0].get_button(2):
                self.boost = False
            if joysticks[0].get_axis(2) > 0.3:
                self.ddx = +math.sin(self.angle * RAD)
                self.ddy = +math.cos(self.angle * RAD)
                self.reverse = True
            else:
                self.reverse = False
        #---End Input---
        Car.update(self, time)

class Player2(Car):
    """Player1 Class"""
    image = player2Car

    def __init__(self):
        Car.__init__(self)
        if len(joysticks) > 1:
            self.joy = True
        else:
            self.joy = False
        self.pos = [1100, 400]
        self.tag = "P2"
        self.image = Player2.image
        self.palette = random.randint(4,5)

    def update(self, time):
        self.ddx = 0.0
        self.ddy = 0.0
        # ---Input Player 1---
        if not self.joy:
            pressedKeys = pygame.key.get_pressed()
            if (pressedKeys[pygame.K_KP0] or pressedKeys[pygame.K_RETURN]) and self.coolDown == 0:
                self.canShoot = True
                self.coolDown = 1
            if pressedKeys[pygame.K_UP]:
                self.ddx = -math.sin(self.angle * RAD)
                self.ddy = -math.cos(self.angle * RAD)
                CarFX(self.rect.center, -self.ddx, -self.ddy, self.palette)
                if pressedKeys[pygame.K_KP_ENTER] or pressedKeys[pygame.K_RSHIFT]:
                    self.boost = True
                    BoostFX(self.rect.center, -self.ddx, -self.ddy)
                if pressedKeys[pygame.K_DOWN]:
                    self.sliding = True
                else:
                    self.sliding = False
            if not pressedKeys[pygame.K_RSHIFT]:
                self.boost = False
            if pressedKeys[pygame.K_DOWN]:
                self.ddx = +math.sin(self.angle * RAD)
                self.ddy = +math.cos(self.angle * RAD)
                self.reverse = True
            else:
                self.reverse = False
            if pressedKeys[pygame.K_LEFT]:
                self.angle += self.newRotateSpeed
            if pressedKeys[pygame.K_RIGHT]:
                self.angle -= self.newRotateSpeed
        #---Joystick Input---
        if self.joy:
            if joysticks[1].get_button(0) and self.coolDown == 0:
                self.canShoot = True
                self.coolDown = 1
            if joysticks[1].get_axis(0) > 0.3:
                self.angle -= self.newRotateSpeed
            if joysticks[1].get_axis(0) < -0.3:
                self.angle += self.newRotateSpeed
            if joysticks[1].get_axis(4) > 0.3:
                self.angle -= self.newRotateSpeed * 0.3
            if joysticks[1].get_axis(4) < -0.3:
                self.angle += self.newRotateSpeed * 0.3
            #---Accelerate and Reverse
            if joysticks[1].get_axis(2) < -0.3:
                if joysticks[1].get_button(1):
                    self.sliding = True
                else:
                    self.sliding = False
                    self.ddx = -math.sin(self.angle * RAD)
                    self.ddy = -math.cos(self.angle * RAD)
                    CarFX(self.rect.center, -self.ddx, -self.ddy, self.palette)
                    if joysticks[1].get_button(2):
                        self.boost = True
                        BoostFX(self.rect.center, -self.ddx, -self.ddy)
            if not joysticks[1].get_button(2):
                self.boost = False
            if joysticks[1].get_axis(2) > 0.3:
                self.ddx = +math.sin(self.angle * RAD)
                self.ddy = +math.cos(self.angle * RAD)
                self.reverse = True
            else:
                self.reverse = False
        #---End Input---
        Car.update(self, time)

class Bullet(pygame.sprite.Sprite):
    """What we shoot"""
    image = BULLET

    def __init__(self, startpos, ang, _tag):
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.pos = startpos
        self.angle = ang
        self.image = Bullet.image
        self.rect = self.image.get_rect()
        self.dx = 0
        self.dy = 0
        self.speed = 6
        self.tag = _tag
        self.counter = 0

    def update(self):
        self.ddx = 0.0
        self.ddy = 0.0
        self.ddx = -math.sin(self.angle * RAD)
        self.ddy = -math.cos(self.angle * RAD)
        BulletFX(self.rect.center, -self.ddx, -self.ddy)
        if self.counter < 60:
            self.dx = self.ddx * self.speed
            self.dy = self.ddy * self.speed
            self.pos[0] += self.dx
            self.pos[1] += self.dy
            self.rect.centerx = round(self.pos[0], 0)
            self.rect.centery = round(self.pos[1], 0)
            self.counter += 1
        else:
            pygame.sprite.Sprite.kill(self)
        GAMEWINDOW.blit(self.image, self.rect)

class Particles(pygame.sprite.Sprite):
    """Base class for particles .... bro"""
    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.pos = [0.0, 0.0]
        self.partMaxSpeed = 5

    def init2(self):
        self.image = pygame.Surface((10, 10))
        self.image.set_colorkey((0, 0, 0))
        pygame.draw.circle(self.image, self.color, (5, 5), random.randint(1,4))
        self.image = self.image.convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.center = self.pos
        self.time = 0.0

    def update(self, seconds):
        self.time += seconds
        if self.time > self.lifetime:
            self.kill()
        self.pos[0] += self.dx * seconds * self.partMaxSpeed
        self.pos[1] += self.dy * seconds * self.partMaxSpeed
        self.rect.centerx = round(self.pos[0], 0)
        self.rect.centery = round(self.pos[1], 0)

class CarFX(Particles):
    """Serious Spark FX"""
    def __init__(self, pos, dx, dy, col):
        if col == 0:
            self.color = Pallete01()
        elif col == 1:
            self.color = Pallete02()
        elif col == 2:
            self.color = Pallete03()
        elif col == 3:
            self.color = Pallete04()
        elif col == 4:
            self.color = Pallete05()
        elif col == 5:
            self.color = Pallete06()
        else:
            self.color = Pallete07()
        self.groups = particleGroup
        self.pos = pos
        Particles.__init__(self, self.pos)
        Particles.init2(self)
        self.pos[0] = pos[0]
        self.pos[1] = pos[1]
        self.lifetime= 0 + random.random()*1.5
        self.partSpeed = 10
        self.partArc = 0.3
        arc = self.partSpeed * self.partArc
        self.dx = dx * self.partSpeed + random.random()*2*arc - arc
        self.dy = dy * self.partSpeed + random.random()*2*arc - arc

class BulletFX(Particles):
    """Moar Serious Spark FX"""
    def __init__(self, pos, dx, dy):
        if random.randint(0,1) == 1:
            self.palette = Pallete03()
        else:
            self.palette = Pallete04()
        self.color = self.palette
        self.groups = particleGroup
        self.pos = pos
        Particles.__init__(self, self.pos)
        Particles.init2(self)
        self.pos[0] = pos[0]
        self.pos[1] = pos[1]
        self.lifetime= 0 + random.random()*0.1
        self.partSpeed = 50
        self.partArc = 1
        arc = self.partSpeed * self.partArc
        self.dx = dx * self.partSpeed + random.random()*2*arc - arc
        self.dy = dy * self.partSpeed + random.random()*2*arc - arc

class BoostFX(Particles):
    """Moar Serious Spark FX for BOOST"""
    def __init__(self, pos, dx, dy):
        self.color = Pallete07()
        self.groups = particleGroup
        self.pos = pos
        Particles.__init__(self, self.pos)
        Particles.init2(self)
        self.pos[0] = pos[0]
        self.pos[1] = pos[1]
        self.lifetime = 0 + random.random() * 0.5
        self.partSpeed = 100
        self.partArc = 0.3
        arc = self.partSpeed * self.partArc
        self.dx = dx * self.partSpeed + random.random() * 2 * arc - arc
        self.dy = dy * self.partSpeed + random.random() * 2 * arc - arc

class KillFX(Particles):
    """Evan Moar Serious Spark FX for MURDER TIME FUN TIME!!!"""
    def __init__(self, pos, dx, dy):
        self.colorChoice = random.randint(0,1)
        if self.colorChoice == 0:
            self.color = Pallete01()
        else:
            self.color = Pallete08()
        self.groups = particleGroup
        self.pos = pos
        Particles.__init__(self, self.pos)
        Particles.init2(self)
        self.pos[0] = pos[0]
        self.pos[1] = pos[1]
        self.lifetime = 1 + random.random() * 2
        self.partSpeed = 75
        self.partArc = 1
        arc = self.partSpeed * self.partArc
        self.dx = dx * self.partSpeed + random.random() * 2 * arc - arc
        self.dy = dy * self.partSpeed + random.random() * 2 * arc - arc

class HitFX(Particles):
    """Evan Moar Serious Spark FX for MURDER TIME FUN TIME!!!"""
    def __init__(self, pos, dx, dy):
        self.colorChoice = random.randint(0,1)
        if self.colorChoice == 0:
            self.color = Pallete08()
        else:
            self.color = Pallete09()
        self.groups = particleGroup
        self.pos = pos
        Particles.__init__(self, self.pos)
        Particles.init2(self)
        self.pos[0] = pos[0]
        self.pos[1] = pos[1]
        self.lifetime = 0 + random.random() * 0.2
        self.partSpeed = 150
        self.partArc = 1
        arc = self.partSpeed * self.partArc
        self.dx = dx * self.partSpeed + random.random() * 2 * arc - arc
        self.dy = dy * self.partSpeed + random.random() * 2 * arc - arc

Car.groups = playerGroup
Bullet.groups = bulletGroup
Particles.groups = particleGroup


### BUTTONS
class Button():
    """Button Class"""
    def __init__(self, imgOff, imgOn, imgPressed, cent=(0,0)):
        self.imageOff = imgOff
        self.imageOn = imgOn
        self.imagePressed = imgPressed
        self.size = imgOff.get_size()
        self.rect = imgOff.get_rect()
        self.rect.center = cent

    def update(self, curs, click):
        cursorPos = curs

        if (self.rect.left < cursorPos[0] < self.rect.right and
            self.rect.top < cursorPos[1] < self.rect.bottom):
            GAMEWINDOW.blit(self.imageOn, self.rect)
            if click == 1:
                GAMEWINDOW.blit(self.imagePressed, self.rect)
                return True
        else:
            GAMEWINDOW.blit(self.imageOff, self.rect)


### FUNCTIONS
def main():
    """Main game engine."""
    global GAMEWINDOW, MAINCLOCK, MIXER, MENUBG, VOLUME

    buttonStart = Button(IMGSTARTOFF, IMGSTARTON, IMGSTARTPRESSED, (GWX * 0.66, GWY - 50))
    buttonQuit = Button(IMGQUITOFF, IMGQUITON, IMGQUITPRESSED, (GWX * 0.66, GWY + 75))
    buttonVolume = Button(IMGVOLUMEOFF, IMGVOLUMEOFF, IMGVOLUMEOFF, (GWX * 0.66, GWY + 200))
    buttonVolumeUp = Button(IMGVOLUMEUPOFF, IMGVOLUMEUPON, IMGVOLUMEUPPRESSED, (GWX * 0.82, GWY + 200))
    buttonVoluemDown = Button(IMGVOLUMEDOWNOFF, IMGVOLUMEDOWNON, IMGVOLUMEDOWNPRESSED, (GWX * 0.5, GWY + 200))

    while True:
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos

        MIXER.set_volume(VOLUME)
        if not MIXER.get_busy():
            MIXER.load(MENUMUSIC)
            MIXER.play(-1)

        # SCREEN REFRESH
        GAMEWINDOW.blit(MENUBG, (0,0))

        if buttonStart.update(mousePos, click1):
            if pygame.event.get(MOUSEBUTTONUP):
                playGame()
        if buttonQuit.update(mousePos, click1):
            if pygame.event.get(MOUSEBUTTONUP):
                quitGame()
        buttonVolume.update(mousePos, click1)
        if buttonVoluemDown.update(mousePos, click1) and VOLUME > 0.0:
            if pygame.event.get(MOUSEBUTTONUP):
                VOLUME -= 0.1
        if buttonVolumeUp.update(mousePos, click1) and VOLUME < 1.0:
            if pygame.event.get(MOUSEBUTTONUP):
                VOLUME += 0.1

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    quitGame()

        GAMEWINDOW.blit(CURSORICON, mousePos)
        pygame.display.update()
        MAINCLOCK.tick(60)

def pointInRect(x, y, rect):
    if (x > rect.left) and (x < rect.right) and (y > rect.top) and (y < rect.bottom):
        return True
    else:
        return False

def collide(rect1, rect2):
    """Checks if rect1 is in rect2"""
    for a, b in [(rect1, rect2), (rect2, rect1)]:
        if ((pointInRect(a.left, a.top, b)) or
            (pointInRect(a.left, a.bottom, b)) or
            (pointInRect(a.right, a.top, b)) or
            (pointInRect(a.right, a.bottom, b))):
            return True
    return False

def inGameMenu(cursorPos, click):
    """Menu overlay during game."""
    buttonMenuQuit = Button(IMGQUITOFF, IMGQUITON, IMGQUITPRESSED, (GWX, GWY+30))
    buttonMenuReturn = Button(IMGMAINMENUOFF, IMGMAINMENUON, IMGMAINMENUPRESSED, (GWX, GWY-30))
    menuWindow = pygame.Surface((500, 250))
    menuWindow.set_alpha(128)
    menuWindow.fill((40, 40, 50))
    GAMEWINDOW.blit(menuWindow, (GWX - 250, GWY - 125))

    if buttonMenuQuit.update(cursorPos, click):
        pygame.time.delay(50)
        if pygame.event.get(MOUSEBUTTONUP):
            return 0
    if buttonMenuReturn.update(cursorPos, click):
        pygame.time.delay(50)
        if pygame.event.get(MOUSEBUTTONUP):
            return 1

def playGame():
    """Shooter Prototype."""
    global GAMEWINDOW, MAINCLOCK, MIXER, VOLUME

    MIXER.stop()
    MIXER.load(GAMEMUSIC)

    menuOn = False
    seconds = MAINCLOCK.tick(60) / 1000
    menuTimer = 30

    count5, count5rect = textObject("5", COUNTFONT, RED)
    count4, count4rect = textObject("4", COUNTFONT, DARK_RED)
    count3, count3rect = textObject("3", COUNTFONT, PURPLE)
    count2, count2rect = textObject("2", COUNTFONT, DARK_GREEN)
    count1, count1rect = textObject("1", COUNTFONT, GREEN)
    destroy, destroyRect = textObject("DESTROY", COUNTFONT, RED)
    destroy2, destroy2Rect = textObject("DESTROY", COUNTFONT, WHITE)
    count5rect.center = (GWX, 250)
    count4rect.center = (GWX, 250)
    count3rect.center = (GWX, 250)
    count2rect.center = (GWX, 250)
    count1rect.center = (GWX, 250)
    destroyRect.center = (GWX, 250)
    destroy2Rect.center = (GWX, 300)
    startCounter = 300
    isRunning = False
    player1Wins, player1WinsRect = textObject("Player 1 Wins !", COUNTFONT, RED)
    player1WinsRect.center = (GWX, GWY - 50)
    player2Wins, player2WinsRect = textObject("Player 2 Wins !", COUNTFONT, BLUE)
    player2WinsRect.center = (GWX, GWY - 50)
    drawGame, drawGameRect = textObject("DRAW GAME", COUNTFONT, PURPLE)
    drawGameRect.center = (GWX, GWY - 50)
    gameOver = False
    winner = 0

    gameTime = 3600

    player1 = Player1()
    player2 = Player2()
    playerGroup.update(seconds)

    while True:
        mousePos = pygame.mouse.get_pos()
        click1, click2, click3 = pygame.mouse.get_pressed()
        cursorRect.center = mousePos

        MIXER.set_volume(VOLUME)
        if not MIXER.get_busy():
            MIXER.play(-1)

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    menuOn = not menuOn

        #---Joystick---
        for joy in joysticks:
            if joy.get_button(7) and menuTimer == 30:
                menuOn = not menuOn
                menuTimer -= 1
            elif 0 < menuTimer < 30:
                menuTimer -= 1
            else:
                menuTimer = 30

        GAMEWINDOW.fill(GREY)
        GAMEWINDOW.blit(GAMEBG, (0,0))

        if isRunning:
            if -300 < startCounter <= 0:
                startCounter -= 1
                if gameTime % 2 == 0:
                    GAMEWINDOW.blit(destroy, destroyRect)
                else:
                    GAMEWINDOW.blit(destroy2, destroy2Rect)

            if menuOn:
                if inGameMenu(mousePos, click1) == 0:
                    quitGame()
                elif inGameMenu(mousePos, click1) == 1:
                    # ---Remove game objects from scene before returning to main menu.
                    playerGroup.empty()
                    bulletGroup.empty()
                    MIXER.stop()
                    return None
                GAMEWINDOW.blit(CURSORICON, mousePos)

            if not menuOn:
                gameTime -= 1
                playerGroup.update(seconds)
                bulletGroup.update()
                particleGroup.update(seconds)
                particleGroup.draw(GAMEWINDOW)

                #---player collision---
                if collide(player1.rectDetect, player2.rectDetect):
                    player1.dx = -player1.dx * 1.5
                    player1.dy = -player1.dy * 1.5
                    player2.dx = -player2.dx * 1.5
                    player2.dy = -player2.dy * 1.5

                #---Wall Collision---
                for wall in wallRects:
                    if collide(wall, player1.rectDetect):
                        player1.dx = -player1.dx * 1
                        player1.dy = -player1.dy * 1
                    if collide(wall, player2.rectDetect):
                        player2.dx = -player2.dx * 1
                        player2.dy = -player2.dy * 1

                #---Bullet Collision---
                for bullet in bulletGroup:
                    if collide(bullet.rect, player1.rectDetect):
                        if not bullet.tag == "P1":
                            player1.dx = -player1.dx * 1
                            player1.dy = -player1.dy * 1
                            player1.health -= 1
                            bullet.kill()
                            for i in range(0,20):
                                HitFX(player1.pos, 0, 0)
                            print("Player 1: " + str(player1.health))
                    if collide(bullet.rect, player2.rectDetect):
                        if not bullet.tag == "P2":
                            player2.dx = -player2.dx * 1
                            player2.dy = -player2.dy * 1
                            player2.health -= 1
                            bullet.kill()
                            for i in range(0, 20):
                                HitFX(player2.pos, 0, 0)
                            print("Player 2: " + str(player2.health))

                #---Is Game Over?
                if player1.health == 0 or player2.health == 0 or gameTime == 0:
                    if player1.health > player2.health:
                        player2.kill()
                        winner = 1
                    elif player2.health > player1.health:
                        player1.kill()
                        winner = 2
                    else:
                        winner = 3
                    gameOver = True
                    isRunning = False
                    startCounter = 300
        #---Not Running---
        else:
            if startCounter > 0:
                startCounter -= 1
                if startCounter > 240:
                    GAMEWINDOW.blit(count5, count5rect)
                elif startCounter > 180:
                    GAMEWINDOW.blit(count4, count4rect)
                elif startCounter > 120:
                    GAMEWINDOW.blit(count3, count3rect)
                elif startCounter > 60:
                    GAMEWINDOW.blit(count2, count2rect)
                else:
                    if gameOver:
                        for player in playerGroup:
                            player.kill()
                    GAMEWINDOW.blit(count1, count1rect)
            if winner == 1:
                GAMEWINDOW.blit(player1Wins, player1WinsRect)
            elif winner == 2:
                GAMEWINDOW.blit(player2Wins, player2WinsRect)
            elif winner == 3:
                GAMEWINDOW.blit(drawGame, drawGameRect)
            if startCounter == 0:
                isRunning = True
                gameOver = False
                gameTime = 3600
                winner = 0
                if len(playerGroup) == 0:
                    player1 = Player1()
                    player2 = Player2()
            #---Update Images While Game Is Loading---
            if gameOver:
                playerGroup.update(seconds)
            else:
                playerGroup.draw(GAMEWINDOW)
            particleGroup.update(seconds)
            particleGroup.draw(GAMEWINDOW)

        ### UI
        #---Health Bars---
        healthBarP1, healthBarP1Rect = textObject("P1 HP: " + str(player1.health), COUNTFONT, RED)
        healthBarP2, healthBarP2Rect = textObject("P2 HP: " + str(player2.health), COUNTFONT, BLUE)
        healthBarP1Rect.center = (GWX * 0.33, 40)
        healthBarP2Rect.center = (GWX * 1.33, 40)
        GAMEWINDOW.blit(healthBarP1, healthBarP1Rect)
        GAMEWINDOW.blit(healthBarP2, healthBarP2Rect)
        #---Timer Display---
        timeDisplay, timeDisplayRect = textObject("Time: " + str(gameTime), COUNTFONT, WHITE)
        timeDisplayRect.topleft = (950, 625)
        GAMEWINDOW.blit(timeDisplay, timeDisplayRect)
        pygame.display.update()
        MAINCLOCK.tick(60)

def quitGame():
    """Exit pygame and system."""
    pygame.quit()
    sys.exit()

### Run Game
if __name__ == "__main__":
    main()
