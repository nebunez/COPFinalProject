COPFinalProject
===============
Group -Awesome!

### Members
* John - <JohnRyanAudio@gmail.com>
* Bryce - <ebryce91@yahoo.com>
* Seldon - <ghyut1@live.com>
* Caleb - <FatalZeus@gmail.com>
* Kirra - <kmtavary@gmail.com>

##[ASANA](https://app.asana.com)

##[Our Project on Github](https://github.com/JohnRyanAudio/COPFinalProject)

---MOTD---
===============
#####<u>I will be updating this MOTD section as we progress to share updates and make announcements as needed.  So, try to check this README as often as possible.</u>

Feel free to store your work in progress code in this repository as a <u>separate</u> file.

<b>i.e.</b> I have a file in the repository now named <i>jrWIP.py</i>

This is the file I will be using to work on my part of the project until it's ready to add to the project code. We should  
all be working on our own <b>'snippets'</b> that can run in a standalone file. Then, we can copy>paste just what we need from our  
snippets into the main file, <i>prototype.py</i>

Let's stick to using <b>camelCase</b> type for our code, since that is what our course has been using. I will have to update the  
prototype skeleton to reflect this, as I've been using the [PEP 8 Style Guide](https://www.python.org/dev/peps/pep-0008). This is a good place to look if you  
are unsure of how to format your code, but I will go over any code committed to the main file for consistency.

Here is the bare-bones code you need to get <b>pygame</b> running in your own files:

    import pygame, sys, random
    from pygame.locals import *
    
    ### PYGAME SETUP
    pygame.init()
    mainClock = pygame.time.Clock()
    
    ### SCREEN SETUP
    SCREEN_INFO = pygame.display.Info()
    WINDOW_WIDTH = 1280
    WINDOW_HEIGHT = 720
    gameWindow = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), 0, 0)
    
    
    """
    YOUR CODE HERE
    """
    
    
    ### RUN ENGINE
    while True:
        for event in pygame.event.get():
            # Check for Quit
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
    
        # SCREEN REFRESH
        pygame.display.update()
        mainClock.tick(60)